module Basic where

-- Exercise 2.1
fib :: Int -> Int
fib n = let
    doFib prev curr 0 = prev
    doFib prev curr n = doFib curr (curr + prev) (n - 1)
    in doFib 0 1 n

-- Exercise 2.2
isSorted :: [a] -> (a -> a -> Bool) -> Bool
isSorted [] _ = True
isSorted [x] _ = True
isSorted (x:y:[]) checker = checker x y
isSorted (x:xs@(y:_)) checker
        | checker x y = isSorted xs checker
        | otherwise = False

-- I think that curry and uncurry are useless in Haskell, what I've done is use a tuple
-- and apply each of its arguments in the function and for uncurry the reverse, get
-- a function with n arguments and create a new one which get a tuple with n elements
-- and apply each one to the right position.

-- Given that all Haskell's functions are curried by default I just had to use create
-- a new function that created the tuple.
-- Exercise 2.3
curry :: ((a, b) -> c) -> a -> b -> c
curry f x y = f (x, y)

-- Exercise 2.4
uncurry :: (a -> b -> c) -> (a, b) -> c
uncurry f (x,y) = f x y

-- Exercise 2.5
compose :: (b -> c) -> (a -> b) -> a -> c
compose g f x = g (f x)