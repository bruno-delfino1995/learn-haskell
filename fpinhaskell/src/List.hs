module List where

import Prelude hiding (
    sum
    , map
    , reverse
    , zipWith
    , drop
    , dropWhile)
import Data.Monoid

infixr 5 `Cons`
data List a = Empty | Cons a (List a) deriving (Show, Eq)

instance Functor List where
    fmap f Empty = Empty
    fmap f (Cons x xs) = Cons (f x) (fmap f xs)

-- instance Foldable List where
--     foldMap f Empty = mempty
--     foldMap f (Cons x xs) = f x `mappend` foldMap f xs

-- instance Monoid (List a) where
--     mempty = Empty
--     mappend xs ys = let
--         doMappend acc Empty = acc
--         doMappend acc (Cons y ys) = doMappend (Cons y acc) ys
--         in reverse' . doMappend (reverse' xs) $ ys

-- Exercise 3.1
match xs = case xs of
    x `Cons` 2 `Cons` 4 `Cons` _ -> x
    Empty -> 42
    x `Cons` y `Cons` 3 `Cons` 4 `Cons` _ -> x + y
    h `Cons` t -> h + sum t
    -- _ -> 101 - This pattern actually overlaps the previous - although if we didn't
    -- have a pattern for Empty it would be considered a pattern for that

y = 3
right = y == match (1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` Empty)

-- Exercise 3.2
tail :: List a -> List a
tail Empty = Empty
tail (Cons x xs) = xs

-- Exercise 3.3
setHead :: List a -> a -> List a
setHead Empty head = Cons head Empty
setHead (Cons x xs) head = Cons head xs

-- Exercise 3.4
drop :: Int -> List a -> List a
drop _ Empty = Empty
drop 0 xs = xs
drop n (Cons x xs) = drop (n - 1) xs
-- tail = drop 1

-- Exercise 3.5
dropWhile :: (a -> Bool) -> List a -> List a
dropWhile f list@(Cons x xs)
    | f x = dropWhile f xs -- will fall to the next pattern if the application returns an error
    | otherwise = list

-- Exercise 3.6
init :: List a -> List a
init x = let
    doInit acc Empty = acc
    doInit acc (Cons x Empty) = acc
    doInit acc (Cons x xs) = doInit (Cons x acc) xs
    in reverse . doInit Empty $ x

-- Exercise 3.7
product :: Num a => List a -> a
product = getProduct . foldLeft mappend mempty . fmap Product

-- Exercise 3.8
-- Something related to Scala that I have no idea what it is all about. Maybe it's
-- something to do with that List needs to be a monoid in order to be the result
-- of a folding

-- Exercise 3.9
length :: List a -> Int
length = getSum . foldLeft mappend mempty . fmap (\_ -> Sum 1)

-- Exercise 3.10
foldLeft :: (b -> a -> b) -> b -> List a -> b
foldLeft _ acc Empty = acc
foldLeft f acc (Cons x xs) = foldLeft f (f acc x) xs

-- Exercise 3.11
sum :: Num a => List a -> a
sum = getSum . foldLeft mappend mempty . fmap Sum

-- Exercise 3.12
reverse :: List a -> List a
reverse = foldLeft (flip Cons) Empty

-- Exercise 3.13
foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight f acc = foldLeft (flip f) acc . reverse

-- Exercise 3.14
append :: a -> List a -> List a
append x = foldRight Cons (Cons x Empty)

-- Exercise 3.15\
flatten :: List (List a) -> List a
-- TODO: Make this more readable
flatten = foldLeft (flip (foldRight Cons)) Empty

-- Exercise 3.16
incrementAll :: Num a => List a -> List a
incrementAll = fmap (+ 1)

-- Exercise 3.17
map :: (a -> b) -> List a -> List b
map = fmap

-- Exercise 3.19
filter :: (a -> Bool) -> List a -> List a
filter f = reverse . foldLeft (\acc x -> if (f x) then Cons x acc else acc) Empty

-- Exercise 3.20
flatMap :: (a -> List b) -> List a -> List b
flatMap f = flatten . map f

-- Exercise 3.21
filter' :: (a -> Bool) -> List a -> List a
filter' f = flatMap (\x -> if (f x) then Cons x Empty else Empty)

-- Exercise 3.22
zipAdding :: Num a => List a -> List a -> List a
zipAdding xs = let
    in zipWith (+) xs

-- Exercise 3.23
-- TODO: Define zipping in terms of foldLeft
zipWith :: (a -> b -> c) -> List a -> List b -> List c
zipWith f first second = let
    doZipWith acc Empty _ = acc
    doZipWith acc _ Empty = acc
    doZipWith acc (Cons x xs) (Cons y ys) = doZipWith (Cons (f x y) acc) xs ys
    in reverse $ doZipWith Empty first second

-- Exercise 3.24
hasSubsequence :: Eq a => List a -> List a -> Bool
hasSubsequence sequence = let
    doHasSubsequence _ Empty Empty = True
    doHasSubsequence _ Empty _ = True
    doHasSubsequence _ _ Empty = False
    doHasSubsequence original (Cons y ys) (Cons x xs)
        | y == x = doHasSubsequence original ys xs
        | otherwise = doHasSubsequence original original xs
    in doHasSubsequence sequence sequence
