module Tree where

import Prelude hiding (max, map)
import qualified Prelude as P

data Tree a = Empty | Node a (Tree a, Tree a) deriving (Show, Eq)

-- Exercise 3.25
size :: Tree a -> Int
size Empty = 0
size (Node _ (left, right)) = 1 + size left + size right

-- Exercise 3.26
max :: (Ord a) => Tree a -> a
max node@(Node x _) = max' x node
    where
        max' curr Empty = curr
        max' curr (Node x (left, right)) = curr `P.max` (max' x left `P.max` max' x right)

-- Exercise 3.27
depth :: Tree a -> Int
depth Empty = 0
depth (Node _ (left, right)) = 1 + (depth left `P.max` depth right)

-- Exercise 3.28
map :: (a -> b) -> Tree a -> Tree b
map f Empty = Empty
map f (Node x (left, right)) = Node (f x) (map f left, map f right)
