module BasicSpec where

import Test.Hspec

import Basic as B

spec :: Spec
spec = do
    describe "fib" $ do
        it "is 0 for 0" $ do
            B.fib 0 `shouldBe` 0
        it "is 1 for 1" $ do
            B.fib 1 `shouldBe` 1
        it "is the sum of past two for the others" $ do
            B.fib 2 `shouldBe` 1
            B.fib 3 `shouldBe` 2
            B.fib 10 `shouldBe` 55
    describe "isSorted" $ do
        it "doesn't check empty lists" $ do
            B.isSorted ([] :: [Int]) (<) `shouldBe` True
        it "doesn't check singleton lists as well" $ do
            B.isSorted [1] (<) `shouldBe` True
            B.isSorted ["a"] (>) `shouldBe` True
        it "needs at least a pair to check" $ do
            B.isSorted [1,2] (<) `shouldBe` True
            B.isSorted [2,1] (<) `shouldBe` False
            B.isSorted [3,-1] (>) `shouldBe` True
        it "checks the sorting using a checker" $ do
            let checker = (<)
            B.isSorted [1,2,3] checker `shouldBe` True
            B.isSorted [1,3,2] checker `shouldBe` False
    describe "curry" $ do
        it "builds the tuple for the function" $ do
            let func = \(x, y) -> x + y
                curried = B.curry func
            curried 1 2 `shouldBe` 3
    describe "uncurry" $ do
        it "decontructs the given tuple for the function" $ do
            let func = \x y -> x + y
                uncurried = B.uncurry func
            uncurried (1,2) `shouldBe` 3
    describe "compose" $ do
        let f = \x -> x + 1
            g = \y -> y * 2
        it "pipes the output from one function into the next" $ do
            let composed = g `compose` f
            composed 1 `shouldBe` 4
        it "can be composed many times" $ do
            let h = \z -> z ^ 3
                composed = h `compose` (g `compose` f)
            composed 1 `shouldBe` 64
