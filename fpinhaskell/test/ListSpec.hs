module ListSpec where

import Test.Hspec

import List as L

list = 1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` Empty
smallList = 11 `Cons` 12 `Cons` Empty

spec = do
    describe "tail" $ do
        -- TODO: Throw an exception on empty list, how to test it?
        it "removes the head of a list" $ do
            let list = 3 `Cons` Empty
                list' = 1 `Cons` 2 `Cons` 3 `Cons` Empty
            L.tail list `shouldBe` Empty
            L.tail list' `shouldBe` (2 `Cons` 3 `Cons` Empty)
    describe "setHead" $ do
        it "changes the current head" $ do
            let list = 3 `Cons` Empty
            L.setHead list 5 `shouldBe` (5 `Cons` Empty)
        it "sets a head for empty lists" $ do
            L.setHead Empty 7 `shouldBe` (7 `Cons` Empty)
    describe "drop" $ do
        it "removes a number of elements from the beginning of the list" $ do
            L.drop 2 list `shouldBe` (3 `Cons` 4 `Cons` 5 `Cons` Empty)
            L.drop 5 list `shouldBe` Empty
        it "stops when hit the end of the list" $ do
            L.drop 10 list `shouldBe` Empty
        it "drops nothing when there is nothing to drop" $ do
            L.drop 2 Empty `shouldBe` (Empty :: List Int)
    describe "dropWhile" $ do
        it "removes elements while the checker returns `True`" $ do
            L.dropWhile (< 4) list `shouldBe` (4 `Cons` 5 `Cons` Empty)
            L.dropWhile (> 5) list `shouldBe` list
    describe "init" $ do
        it "gets everything but the last element" $ do
            L.init list `shouldBe` (1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` Empty)
            L.init (1 `Cons` 2 `Cons` 3 `Cons` Empty) `shouldBe` (1 `Cons` 2 `Cons` Empty)
        it "returns an empty list when working on an empty list" $ do
            L.init Empty `shouldBe` (Empty :: List Int)
    describe "product" $ do
        it "calculates the product of the list elements" $ do
            L.product list `shouldBe` 120
            L.product (1 `Cons` 3 `Cons` 9 `Cons` Empty) `shouldBe` 27
        it "is 1 for an empty list" $ do
            L.product Empty `shouldBe` 1
    describe "sum" $ do
        it "sums the list elements" $ do
            L.sum list `shouldBe` 15
            L.sum (13 `Cons` 21 `Cons` Empty) `shouldBe` 34
        it "is 0 for an empty list" $ do
            L.sum Empty `shouldBe` 0
    describe "length" $ do
        it "is 0 for an empty list" $ do
            L.length Empty `shouldBe` 0
        it "calculates the length of a list" $ do
            L.length list `shouldBe` 5
    describe "foldLeft" $ do
        it "applies the accumulator through the whole list using the previous value" $ do
            L.foldLeft (\acc x -> acc) 0 list `shouldBe` 0
        it "ends on the end of the list" $ do
            L.foldLeft (\acc x -> x) 0 list `shouldBe` 5
        it "pass the accumulated and then the current value" $ do
            L.foldLeft (-) 0 list `shouldBe` -15
    describe "foldRight" $ do
        it "pass the current and then the accumulated value" $ do
            L.foldRight (-) 0 list `shouldBe` 3
        it "applies the accumulator through the whole list using the previous value" $ do
            L.foldRight (\x acc -> acc) 0 list `shouldBe` 0
        it "ends on the beginning of the list" $ do
            L.foldRight (\x acc -> x) 0 list `shouldBe` 1
    describe "reverse" $ do
        it "reverses the list" $ do
            L.reverse list `shouldBe` (5 `Cons` 4 `Cons` 3 `Cons` 2 `Cons` 1 `Cons` Empty)
            L.reverse (1 `Cons` 2 `Cons` Empty) `shouldBe` (2 `Cons` 1 `Cons` Empty)
        it "does nothing to singleton lists" $ do
            L.reverse (70 `Cons` Empty) `shouldBe` (70 `Cons` Empty)
        it "returns an empty list if an empty has been given" $ do
            L.reverse Empty `shouldBe` (Empty :: List Int)
    describe "append" $ do
        it "adds an element to the end of a list" $ do
            L.append 5 list `shouldBe` (1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` 5 `Cons` Empty)
            L.append 6 (1 `Cons` Empty) `shouldBe` (1 `Cons` 6 `Cons` Empty)
        it "adds a head to an empty list" $ do
            L.append 8 Empty `shouldBe` (8 `Cons` Empty)
    describe "flatten" $ do
        it "removes the intermediate lists of a list" $ do
            L.flatten (list `Cons` list `Cons` Empty) `shouldBe` (1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` 1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` Empty)
            L.flatten ((L.reverse list) `Cons` list `Cons` Empty) `shouldBe` (5 `Cons` 4 `Cons` 3 `Cons` 2 `Cons` 1 `Cons` 1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` Empty)
        it "removes empty lists from the middle of a list" $ do
            L.flatten (list `Cons` Empty `Cons` list `Cons` Empty) `shouldBe` (1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` 1 `Cons` 2 `Cons` 3 `Cons` 4 `Cons` 5 `Cons` Empty)
    describe "incrementAll" $ do
        it "adds 1 to every element of the list" $ do
            L.incrementAll smallList `shouldBe` (12 `Cons` 13 `Cons` Empty)
    describe "map" $ do
        it "doesn't alter the content by itself" $ do
            L.map id list `shouldBe` list
        it "pass the mapper throughout the list" $ do
            L.map (\x -> x - 1) smallList `shouldBe` (10 `Cons` 11 `Cons` Empty)
    describe "filter" $ do
        it "removes elements that doesn't pass the filter" $ do
            L.filter odd list `shouldBe` (1 `Cons` 3 `Cons` 5 `Cons` Empty)
            L.filter even list `shouldBe` (2 `Cons` 4 `Cons` Empty)
    describe "flatMap" $ do
        it "appends the returned list to the result" $ do
            L.flatMap (\x -> (x `Cons` x `Cons` Empty)) smallList
                `shouldBe` (11 `Cons` 11 `Cons` 12 `Cons` 12 `Cons` Empty)
    describe "zipAdding" $ do
        it "adds the elements from list one to two, resulting in a new list" $ do
            L.zipAdding smallList smallList `shouldBe` (22 `Cons` 24 `Cons` Empty)
    describe "zipWith" $ do
        it "uses a function to create a new list from the two given" $ do
            L.zipWith (+) smallList smallList `shouldBe` (22 `Cons` 24 `Cons` Empty)
        it "stops when one of the lists end" $ do
            L.zipWith (+) list smallList `shouldBe` (12 `Cons` 14 `Cons` Empty)
            L.zipWith (+) smallList list `shouldBe` (12 `Cons` 14 `Cons` Empty)
    describe "hasSubsequence" $ do
        it "considers that every list contains an empty" $ do
            L.hasSubsequence Empty list `shouldBe` True
        it "returns False for empty list but True when compared to another empty list" $ do
            L.hasSubsequence list Empty `shouldBe` False
            L.hasSubsequence Empty (Empty :: List Int) `shouldBe` True
        it "considers that the list is an subsequence of itself" $ do
            L.hasSubsequence list list `shouldBe` True
        it "checks if the list has a subsequence no matter the position" $ do
            let inTheBeginning = (1 `Cons` Empty)
                inTheMiddle = (2 `Cons` 3 `Cons` 4 `Cons` Empty)
                inTheEnd = (4 `Cons` 5 `Cons` Empty)
            L.hasSubsequence inTheBeginning list `shouldBe` True
            L.hasSubsequence inTheMiddle list `shouldBe` True
            L.hasSubsequence inTheEnd list `shouldBe` True
