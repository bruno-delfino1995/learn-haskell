module TreeSpec where

import Test.Hspec

import Tree as T

smallTree = Node 1 (
    Node 2 (Empty, Empty)
    , Node 3 (Empty, Empty)
    )

tree = Node 10 (
    Node 11 (smallTree, smallTree)
    , Node 12 (smallTree, smallTree)
    )

complexSmallTree = Node 1 (
    Node 2 (
        Empty
        , Node 3 (Empty, Empty)
        )
    , Empty
    )

complexTree = Node 1 (
    Empty
    , Node 2 (
        Node 3 (
            Node 4 (
                Node 5 (Empty, Empty)
                , Node 6 (
                    Node 7 (Empty, Empty)
                    , Node 8 (
                        Node 9 (Empty, Empty)
                        , Empty
                        )
                    )
                )
            , Empty
            )
        , Empty
        )
    )

spec :: Spec
spec = do
    describe "size" $ do
        it "returns the number of leafs in a tree" $ do
            T.size smallTree `shouldBe` 3
            T.size tree `shouldBe` 15
            T.size Empty `shouldBe` 0
    describe "max" $ do
        -- TODO: Throw an exception on empty tree, how to test it?
        it "finds the maximum number in a list" $ do
            T.max tree `shouldBe` 12
            T.max smallTree `shouldBe` 3
    describe "depth" $ do
        it "calculates the maximum depth of a tree" $ do
            T.depth tree `shouldBe` 4
            T.depth complexSmallTree `shouldBe` 3
            T.depth complexTree `shouldBe` 7
    describe "map" $ do
        it "applies a function to every element of the tree" $ do
            T.map (+ 1) smallTree `shouldBe` (Node 2 (Node 3 (Empty, Empty), Node 4 (Empty, Empty)))