import GHC.Int
-- ## What are types?
-- Types are a way to group a set of values that have something in common, abstract or not.
-- The understanding of types can be easier if you already know about sets in mathematics,
-- that knowledge will help knowing how types work in a mathematical sense.
-- To define a type in Haskell we use *data declarations*, which have a type constructor and
-- data constructors. The former being responsible for defining what the type is,
-- signalizing in type signatures the type which is accepted. And the latter is responsible
-- for contructing values of such type, they are the values which inhabit the type, they
-- are used at *term level* (expressions) instead of *type level* (type signatures).
-- Below you can find the data declaration for the boolean type, which can be either
-- true of false. Through a data declaration we create a set of values named by the
-- type constructor and containing the values defined by the data constructors
data Bool' = True' | False'
-- By the above definition we can see that the type is named `Bool'`, so the type signatures
-- will have `Bool'` as the accepted type, and we can see the data constructors separated
-- by `|` meaning that we can construct values of type Bool' using `True'` or `False'`
-- constructors. The pipe `|` indicates a *sum type*, a logical disjunction (or),
-- meaning that a Bool type can be *True **or** False*.
-- These constructions (data declarations) share a common pattern but not precisely,
-- some of them can declare the data constructors using a logical conjunction (and),
-- or the type constructor and the data constructors may receive some arguments. However
-- they always follow the common pattern which is, the *data* keyword followed by the
-- type constructor with then an equals sign and the data constructors.
-- If you want to see a declaration of some type on GHCi you can use the :info command
-- followed by the type that you want information, e.g., `:i Bool` will give you
-- `> data Bool = True | False`

-- Exercises: Mood Swing

data Mood = Blah | Woot deriving Show

-- 1. Type Constructor Name = Mood

-- 2. Possible Values = [Blah, Woot]

-- 3. Signatures are based on type constructors and not data constructors, the correct
-- signature would be `changeMood :: Mood -> Mood`

-- 4 & 5.
-- Here we have used a utility from Haskell which is called 'pattern matching', with
-- it we are able to make assertions on our declarations by specifing values instead
-- of variables, then if the values match with the provided, that declaration gets
-- executed. In the first definition we used our data constructor to match for Blah
-- values, so if you provide a Blah to changeMood it'll execute the following definition
-- and return you a Woot. Otherwise, if you provide anything else, that declaration
-- won't match and Haskell will continue to search further until it hits a matching
-- declaration, which can be one using the catch-all (_) or a variable instead of a value.
-- In our case we have used the catch-all, so if we provided any other Mood than Blah
-- we'll be executing the second declaration
changeMood :: Mood -> Mood
changeMood Blah = Woot
changeMood _ = Blah

-- ## Numeric types

-- Haskell as many other languages uses more than one type for numbers, some with less
-- or more information. They are:
-- 1. Integral Numbers: Whole numbers, positive and negative.
--  1.1. Int - Fixed-precision integer, has a maximum and minimum value
--  1.2. Integer - Supports arbitrarily large or small numbers
-- 2. Fractional: Non integral numbers.
--  2.1. Float - Single-precision floating point numbers. They shift the amount of bits
--   used for the decimal and floating part, thus violating some arithmetical assumptions,
--   making it not suitable for business applications
--  2.2. Double - Double-precision floating point numbers. Use the double of bits that
--   Float uses to describe numbers
--  2.3. Rational - Fractional numbers representing a ratio. It carries two numbers,
--   one for the numerator and another for the denomination (1 / 2 :: Rational). It is
--   arbitrarily precise but as efficient as Scientific.
--  2.4. Scientific - Almost arbitrary precision scientific number and space efficient.
--   It represents numbers in a scientific notation by using a coefficient as Integer
--   and a exponent as Int, but once Int has limit, those limits act upon this type as well.
--   But once the Int is used for the exponent, it's very unlikely to hit such limit.

-- All of those types implement the Num typeclass which means that all of them share a
-- common functionality. The Num typeclass provides the basic operator for mathematical
-- equations (+, -, *), meaning that any type that has provided an instance can be used
-- by those functions.

-- ### Integral Numbers
-- When you put a number in your code it'll automatically be one of those types,
-- Haskell will try to choose the one that suits you best, but sometimes we need
-- to explicitly declare it, either because it has a special notation as the Rational
-- type or just because we want a specific one because of some optimizations. For doing
-- so we use the same syntax as when declaring type signatures, as you can see below
-- we have declared a Int8 type for the first and a Int16 for the last by using the
-- same number. However be carreful, some numerical types have limits while other don't,
-- as is with the Int type (has limit) and Integer (no limit). For the Int8 type,
-- as the name states it uses 8 bits for storing numbers, so we are restricted to
-- use numbers between -2 ^ 7 and 2 ^ 7 (the types uses 8 bits but once it's signed,
-- the most significant bit for the sign) and if we pass the greater limit, we start
-- from the bottom most number, if we assign 128 to a Int8, the resulting number will be
-- -127. OBS: The specific Int types are into GHC.Int
a = 127 :: Int8
b = 128 :: Int16

-- Given that numbers have limits (boundaries), we can use the functions
-- `minBound` and `maxBound` to know its limits. Such function are polymorphic by the
-- return and Haskell assigns a concrete type only when it needs so, thus we need to
-- specifically tell it from which type we want to get the boundaries. Both functions
-- are defined in the Bounded typeclass, so if can only get boundaries of types which
-- have an instance for Bounded (if you have doubts just type `:info <type>` on GHCi )
lowerLimit8 = minBound :: Int8
upperLimit16 = maxBound :: Int16

-- ### Fractional Numbers
-- The three basic factional number types (Float, Double, Rational) are all available
-- in your GHC, while the Scientific type comes from a library. The two first doesn't
-- have an arbitrary precision, meaning that they don't handle a high degree of precision,
-- while the two latter have it, so we can use a number with many numbers after the
-- decimal point.
-- Some computations as the division (`/`) need to receive Fractional numbers instead
-- of Integral numbers. You can see such restriction by looking at the type of `/`
-- which is `(/) :: Fractional a => a -> a -> a`, the `Fractional a =>` part is used
-- to denote a typeclass constraint, this means the type variable `a` must be an
-- instance of the typeclass `Fractional`.
-- You may think that once a type is Fractional it can't be Num, however we can
-- implement as many typeclasses for a type as we want we just can't implement the
-- same twice. Despite of that, we can even specify a typeclass constraint for another
-- typeclass, this way forcing a type to implement one typeclass to be able to implement
-- the other (Fractional does this by forcing types to implement Num, you can see it
-- by typing `:i Fractional` and looking at the line which starts with `class`)
fractionalsAreNums = (1 / 2 :: Rational) + 1.67
-- One interesting thing is that we can use a Num type inside a Fractional computation
-- Haskell will default that type to a Double, so 1 / 2 results in 0.5 instead of
-- an error because it converts the Integral numbers to the Double Fractional. Also,
-- if we do a division with no decimal point in the result, we still get a Fractional
-- once the type signature says so, 4 / 2 results in 2.0
numsAreDefaultedToDouble = 1 / 2
theReturnIsAlwaysFractional = 4 / 2

-- ### Comparing values

-- As any other programming language, Haskell has the ability to compare numbers,
-- but notice the difference in the unequal sign, instead of `!=` as in many languages,
-- Haskell uses `/=` for checking difference between values. When looking at the type
-- declaration for these operations we can see that we have the `Eq a` type constraint,
-- meaning that only types which have an instance for Eq can be compared, however this
-- typeclass deals only with value comparision for equality. If we try comparing if a
-- value is greater than other through the `<` operator we'll see that the type constraint
-- is different, mainly because the concept of greater of smaller is inherent to ordering
-- values, thus we have the `Ord` typeclass defining comparisions that deals with such
-- concept.
-- Take a closer look to the type definition of `== :: Eq a => a -> a -> Bool`, by
-- taking the Eq typeclass as a base for the calculations we gain flexibility (it's a type
-- of polymorphism) once any type which have an instance for Eq can be compared with
-- another of the same type - the same applies for `<`, `>` and so on. Given that these
-- typeclasses determine what can be compared, we can compare almost everything, just
-- need to keep a closer look at the `[]` instance. Through its instance definition
-- `instance Ord a¹ => Ord [a]` we see a type constraint¹ on the instance itself,
-- which means that the instance of [] can only be ordered if its components `a`
-- can be ordered as well.
canBeOrdered = [1, 2, 3] < [1, 2, 4] -- True

data NewMood = G | B deriving Show
-- cannotBeOrdered = [G, B, G] > [G, G, G] -- No instance for (Ord Modd)

-- As we can derive `Ord` and it defines the ordering by the order you've defined
-- your data constructors, so by the following we get that G' < B'
data Mood' = G' | B' deriving (Show, Eq, Ord)
canBeOrdered' = [G', B', G'] > [G', G', G'] -- True

-- ### Exercises - Find the Mistakes
-- 1. not True && True -- data constructors must start with an uppercase letter
-- 2. not (x == 6) -- `=` is the binding/assignment operator, for equality checks we use `==`
-- 3. ok
-- 4. ["Merry"] > ["Happy"] -- I've used String only because I don't know to which type
--  those data constructors belong
-- 5. ['1', '2', '3'] ++ "look at me!" -- Lists are homogenous in Haskell, there's no way
--  to construct a list with two data constructors from different types once the `[]`
--  declaration allows only one through its parameter `data [] a = [] | a : [a]`

-- ### Conditionals with if-then-else
-- In Haskell we have if as expressions instead of statements like in other languages,
-- meaning that once they are expressions we can reduce them to values, while statements
-- can't. To explain that run `if False then "Truthin" else "Falsin"` in GHCi, and an
-- equivalent (`if (false) { "Truthin" } else { "Falsin" }`) on a Xrepl for example; you'll
-- see that GHCi returns a value for its expressions, showing that it was reduced to a value,
-- while Xrepl won't display a thing for the return value once we have if statements
-- and not if expressions in Xlanguage. While testing I saw an interesting behavior,
-- NodeJS can evaluate an if statement to a value when you it on its own (just the if statement),
-- but when trying to assign that same statement to a variable, reducing to the value,
-- we see that it can't; so we can exemplify by saying that when something can be
-- assigned to a variable it's an expression, otherwise, it's a statement. For everything
-- regarding the handling of the condition, Haskell pretty much follows the standards,
-- once the condition evaluates to a Bool it's will take the relative path.
greetIfCool :: String -> IO ()
greetIfCool coolness =
    if cool coolness then
        putStrLn "eyyyyyy. What's shakin'?"
    else
        putStrLn "pshhhhh."
    where
        cool v = v == "downright frosty yo"
-- Given that the function call for cool with coolness as argument reduces to a Bool
-- value we can use it on a if expression. Also note the indentation, it follows the
-- same strict rules as indentation for `where`

-- ## Tuples

-- Tuple is a type that allow us to pass more than one value within the same container,
-- however you may think that List already does such thing, but the main difference with
-- List is that Tuples have a fixed size and are heterogenous. Tuple uses the `,` as
-- its data constructor, and for the type constructor as well; meaning that they use
-- the same syntax for term and type level.
-- Given that tuples have a fixed size, we usually refer to them based on how many
-- values it holds (known as tuple's arity): two-values is a pair, three-values
-- a triple, ...; and once each value of it can be of a different type, we say 
-- that a tuple is a product type (you must apply all its type arguments to create
-- a concrete type) - `data (,) a b = (,) a b`.
validTuple = (,) 1 2 -- has all its arguments bound to specific values (and both type variables bound to `Num a`)
-- doesn'tworks = (,) 3 
-- Considering `(a, b)` a pair, we see that we have the type *a* for its first value
-- and *b* for the second, then the number of possible combinations is the result of
-- how many values (inhabitants of a - M) *a* times how many values (inhabitants of b - N)
-- *b* can hold, so in the end we'd have `M * N` combinations.
-- While in a sum type we have only the sum of the inhabitants, mainly because we
-- have to choose between one or another of the multiple representation. However,
-- the de facto sum type is the `Either` type, because it really sums the amount of
-- one inhabitants with the amount of the inhabitants of the second; its declaration
-- is as follows `data Either a b = Left a | Rigth b`, analysing it we see that
-- we have `M + N` possibilities of values once we need to choose between `Left`
-- and `Right`. Meanwhile, in a commom *sum type* we have only a handful options
-- to choose from (removing the sum in the end), e.g., `Bool` is sometimes called a
-- sum type, however we have only 2 options instead of `M + N` options, but given
-- that sum types are considered types that have multiple representations (separated
-- by `|` on its declaration)
-- But what really allows it to be a product type is that its type declaration has
-- variables (*a* and *b*) which need to be applied to concrete types (the same apply
-- to *Either*, the variables are what makes it the de facto sum type). Those variables
-- on the type level need to be applied to concrete types (a full applied type, a type that
-- has received its type variables), while on the term level they need to be applied to
-- values.
firstBool :: (Bool, b) -> Bool
firstBool (a, _) = a

secondBool :: (a, Bool) -> Bool
secondBool (_, b) = b
-- On these example we can see that we can construct, therefore pattern matching,
-- it using (x, y), both at the term and type level (once the declaration specifies so).
-- Also, once we haven't used one of its components, we can leave its type unapplied,
-- leaving it for the type system to further apply it.
-- Just keep in mind that once tuples are of fixed sizes (thus for constructing
-- it we need to apply all, and for pattern matching as well) we should keep its
-- size small, both for efficiency and sanity (no one wants to write (,,,,,,,,,,,,,,,)
-- with all the types defined)

-- ## Lists

-- Lists likely tuples are a way to wrap many values as a single one. However, they
-- differ from tuples on its type constructor, which is `[]` and used both in term
-- and type level as tuples; they have an arbitrary size, and can only hold values
-- of the same time (they are homogenous) - these are the three main differences
-- between lists and tuples.
emptyList = []
singletonList = [1]
homogenousList = ["hello", " ", "world", "!"]
-- heterogenousList = ["hau", 4, 'y', 0, 'u'] -- this doesn't work
-- The homogeinity of lists is defined on its constructor - `data [] a = [] | a : [a]`
-- - in which we can see that it accepts only one type parameter, unlikely tuples which
-- define more than one. So when defining a function we need to provide, or not,
-- that parameter, if we declare the function will be specific for that type of list,
-- otherwise it's free to go with any type, which is the case for `++` - 
-- `:t (++) => (++) :: [a] -> [a] -> [a]` - and on the counterpart we have `unlines`
-- from `Data.String` - `:t unlines => unlines :: [String] -> String` 

-- ## Chapter Exercises

awesome = ["Papuchon", "curry", ":)"]
also = ["Quake", "The Simons"]
allAwesome = [awesome, also]

-- 1. length :: [a] -> Int

-- 2.
--  a. 5
--  b. 3
--  c. 2
--  d. 5

-- 3. First works because we have two `Num a`, which somehow can be converted to
-- `Fractional a`, however the second doesn't because `length` returns an `Int`,
-- which doesn't have an instance for `Fractional` (it doesn't make sense to do so,
-- an Int can't be a Fraction)

-- 4. We need to convert `Int` to a type which has an instance or can be converted
-- to `Fractional`. For that we need to combine `toInteger` and `fromInteger`, the
-- first is a *Prelude* function that converts *Integral* values to *Integer* and the
-- later is a function available for types that have *Num* instances which converts
-- an *Integer*  into a *Num a*, then having a `Num a` that can be converted to `Fractional a`.
-- Resulting into `6 / fromInteger (toInteger (length [1, 2, 3]))

-- 5. Bool - True

-- 6. Bool - False

-- 7.
--  a. Works because both ends of `==` have same type once length is applied, this
--    will be reduced to True
--  b. Doesn't work because lists are homogeous
--  c. Works because `+` works on top of `Num a` and has a lower precedence than
--    function application, so when length is applied and reduces to an *Int* that
--    *Int* is then added to the other once it has an instance for *Num*, this will
--    be reduced to 5
--  d. Works because both ends reduce to Bool, this will be reduced to False once
--    'b' is greater than 'a'
--  e. Doesn't work because 9 isn't a Bool

-- 8.
-- `isPalindrome` for strings only
-- isPalindrome :: String -> Bool
-- isPalindrome str = str == rts
--   where rts = reverse str
-- In the following signature we need to put the type constraint Eq a for a because
-- we need to be sure that `a` can be compared, which is itself a constraint of
-- the Eq instance for []
isPalindrome :: Eq a => [a] -> Bool
isPalindrome list = list == tsil
  where tsil = reverse list

-- 9.
abs' :: (Num a, Ord a) => a -> a
abs' n = if n < 0 then negate n else n

-- 10.
f :: (a, b) -> (c, d) -> ((b, d), (a, c))
f a b = ((snd a, snd b), (fst a, fst b))

-- ### Correcting syntax

-- 1.
-- This will transform x as an prefix form for +, so we can't use x in the middle of
-- an equation, only in the beginning as any infix function
x = (+)
-- F is an invalid name for function, capital letters in the beginning are reserved
-- for types
add1toLength xs = x w 1
  where w = length xs

-- 2.
-- The author has tried to write an anonymous function, however as we haven't seen
-- it yet, I'm using a simple declaration. Although, here is the definition `\x -> x`
id' x = x

-- 3.
f' (a, b) = a

-- ### Match the function names to their types

-- 1. c - `show :: Show a => a -> String` 
-- 2. b - `(==) :: Eq a => a -> a -> Bool`
-- 3. a - `fst :: (a, b) -> a
-- 4. d - `(+) :: Num a => a -> a -> a

-- ## Names and variables

-- In Haskell there're seven kinds of things that we can name: functions, term-level
-- variables, data constructors, type variables, type constructors, typeclasses, and
-- modules. Things at the term level (data constructors, term-level variables) are
-- the house for our values and what's executed in runtime. While things at the type
-- level (type variables,  type constructors, typeclasses, functions) are used during
-- static analysis and verification of the program. Lastly, for organizing code we
-- have modules.
-- Given those categories which we can name, we should follow some conventions that
-- were being developed through time. For type variables we use a, b, c, ... trying
-- to keep them under f. That's because we use f, g, h, ... for functions when used
-- in term-level variables. However I don't really buy that rule, function names should
-- be self-explanatory. For function parameters it's usually used x, y, z and when they
-- are lists we append a s to it. Therefore, keep in mind that these are just conventions
-- not rules, when the matter is naming, IMHO, we should use descriptive names unless
-- it's use is very clear in the body of the function.

