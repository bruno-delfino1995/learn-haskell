-- When working with the GHCi (Haskell's REPL) we often see `Prelude>`. This denotes
-- the modules that were imported in the current session. The *Prelude* module is
-- a set of standard functions which are imported by default (but can be turned off).
-- In order to have such module you need to have at least the Haskell's base package.

-- The REPL has a lot of features that can be used through specific commands, which
-- each of them start with `:` followed by its name. For loading a file we have the
-- command `:load filename.hs`, as you can see, some commands can receive parameters.
-- After this we see that now the ""PS1"" is `*Main>` which mean that we have imported
-- the Main module into our session, but the Preload remains. By loading the file
-- we now have all of the definitions available for use, so if you type `greeting "Your name"`
-- you will see that the string "Hello, Your name!" is printed to the console.

-- Below you can see a common Haskell definition/declaration, a type signature followed
-- by the real definition. It's a good practice to explicitly define the type signature
-- in Haskell, although the compiler can infer the types.
-- A type signature is composed by `name :: type`, you can read the `::` as *has type of*,
-- so below we can read as 'greeting as a type of `String -> IO ()`.
-- Underneath the type signature we have the actual definition, which is formed by
-- `name [param1 param2 param3] = expression` and must match the given signature.
greeting :: String -> IO ()
greeting name = putStrLn ("Hello, " ++ name ++ "!")

-- Everything in Haskell is an expression, and declarations are like bindings to name
-- expressions. The following are all expressions:
-- ```
-- 1
-- 1 + 1
-- "Someone"
-- ```
-- Expressions can be raw used only in the REPL, source files must contain only
-- definitions, if you try to load a file with a raw expression on it GHC will comply.
-- When inserting expressions in the REPL it will try to reduce it to the normal form,
-- in other words, it will apply all of the arguments until it reaches a point where
-- no more evaluation can be done. The normal form of `1 + 1` is 2 because it's the
-- application of two parameters (1, 1) to the + function/operator, resulting in 2.

-- The below code snippet is another building block of Haskell - functions. As in
-- lambda calculus, functions in Haskell receive one argument and return one value.
-- Some functions may receive no value and always return the same value, acting as
-- constants. Although, one fact you have to keep in mind is that given the same
-- input, the function must return the same value. Functions in Haskell map exactly
-- to what we have in Math. You can think of functions as just named parametrized expressions.
-- When a function has more than one argument, you may think that we apply two
-- arguments and finally execute it. What really happens is that the function is
-- applied to the first argument and return a new function that gets applied to the
-- second value, reaching a point were the term is irreducible. This process
-- of splitting a function of n arguments into n function with only one argument is
-- called currying.
-- **OBS:** Function and parameter names always start with lower case letters
concatStr :: String -> String -> String
concatStr str1 str2 = str1 ++ str2

-- When applying a function we say that we are evaluating its body for the given
-- parameters. However, unlikely the other languages were this happens right ahead
-- the application, Haskell uses a process similar to beta reduction, where it
-- binds the arguments to each of the parameters in the function's body and pass that
-- ahead, until somewhere is asked to do a full evaluation. This feature is called
-- lazy evaluation, making possible to create infinite lists to be evaluated only
-- when asked, preventing an infinite loop. And this is possible only because as Haskell
-- is pure, so all functions are pure, automatically means that we have referential
-- transparency, which means that if we have a function `add1 x = x + 1` we can
-- change its application anywhere for its body with the argument applied, e.g.,
-- if we have `add1 2 + 4` we can substitute the add for its expression with the
-- parameters bounded, resulting in `add1 2 + 4 => 2 + 1 + 4 => 7`.

-- Exercises: Comprehension Check
-- 1 -> Applicable only for GHC < 8.0.1, for the newer versions the `let` isn't required
-- let half x = x / 2
-- let square x = x * 2

-- 2 & 3
circleArea r = pi * r ^ 2

-- For the concatStr function you may have asked yourself how is it possible to use
-- `++` as if it were an operator whilst in Math it isn't. This is possible due to
-- infix functions, functions that can be used as if it were an operator.
-- By default all functions are prefix, as in `id 1`, `concatStr "Hello, " "World"`,
-- `circleArea 2`...; although you can use any binary function as an operator by using
-- the tick caracter \`, as in `div 10 4` turns into `10 `div` 4`. On the other hand
-- you the opposite for infix functions, by wrapping them around parenthesis you turn
-- them into prefix functions (this is possible due to currying, you applied no parameter
-- so you get the whole function back), so `1 + 2` turns into `(+) 1 2`
-- Below you can see that I used the parenthesis to create a prefix functions, and has
-- used the returned function as the value for concatStr', creating a function in
-- what we call a point-free style. But when doing that for a generic function always
-- remember that now concatStr' is just an alias for ++, which means that we can
-- concat any array using concatStr', the type signature for concatStr' is the same
-- as ++, which is `[a] -> [a] -> [a]`. To prevent that we should use type signature
-- signaling the correct types that should be used.
concatStr' :: String -> String -> String
concatStr' = (++)

-- Now you may ask yourself what about the order, can I use `++` of `div` in the middle
-- of anything? In which order will it be applied?
-- That can be checked using `:info x` which gives back the information of any
-- Haskell term. When asking for the info of `+`, `-`, `*` we get back its associativity
-- and precedence, meaning how its multiple applications are grouped, and the order of evaluation
-- which ranges from 0-9 where we can define until 8 and 9 is the precendence for
-- function application, meaning that a function is applied before all.
-- `:i (+)` yields `infixl 6`, meaning that it groups by left and is the 4th operation
-- to be executed, for `:i (^)` we have `infixr 8`, groups by right and is the 2th
-- operation to be executed. Just keep in head that the grouping is done based on the
-- precedence, so we can't group an add operation before a power operation, the
-- following alpha equivalence doesn't hold true `1 + 2 ^ 4 == ((1 + 2) ^ 4)`, the
-- real equivalence is `1 + 2 ^ 4 == (1 + (2 ^ 4))`.
-- If you have doubts about which is the operator's associativity, just to the
-- grouping by yourself and see if the results are equal, e.g., `2 ^ 3 ^ 4 == (2 ^ (3 ^ 4))`
-- as the `^` function is right associative. Although some functions may trick you
-- as the `*` does, because it's a commutative operation we can't say for sure
-- that it's right or left associative by just grouping the terms, so the following
-- holds true `2 * 3 * 4 == (2 * (3 * 4)) == ((2 * 3) * 4)`, when actually only the
-- latter is true because `*` is left associative.

-- Exercises: Parentheses and Association
-- 1 a != b -> 8 + 7 * 9 == 8 + (7 * 9)
-- 2 a == b
-- 3 a != b -> x / 2 + 9 == (x / 2) + 9

-- Haskell code seems like Python where indentation matters to signal blocks of
-- code. The rule is pratically the same as Python, code that belongs to an expression
-- must be indented further than the beginning of it. Although Haskell matches the
-- beginning of the expression with the parts that belong to it, so if you misallign
-- the `y` from the `x`, the code won't compile
z = let x = 2
        y = 3
    in x + y

-- Bad examples
-- ```
-- z = let x = 2
--       y = 3
--     in x + y
-- z = let x = 2
--          y = 3
--     in x + y
-- ```

foo x = let y = x * 2
            z = x
                ^ 2
        in 2 * y * z

-- Exercises: Heal the Sick
-- 1
area x = 3.14 * (x * x)
-- 2
double x = x * 2
-- 3
x = 7
y = 10
f = x + y

-- In Haskell we have two functions for getting the rest of a division, which are
-- `rem` and `mod`, the two are very similar but mod is more suitable for cases
-- where your divisor is like a numerical system which starts at 0. Like when calculating
-- hour/week day, having the clock/week days as the divisor, e.g, trying to know
-- which week day will be from 23 days starting at monday
twentyTreeFromMonday = (1 + 23) `mod` 7
twentyTreeFromMonday' = (1 + 23) `rem` 7

-- As you can see above, both cases will give the same result. But what about 23 days
-- before Monday? The key difference for rem and mod is that the sign of the result
-- for rem will have the same sign as the dividend, and for mod the same sign as the
-- divisor
twentyTreeBeforeMonday = (1 - 23) `mod` 7
twentyTreeBeforeMonday' = (1 - 23) `rem` 7 -- There is no week day numbered as -1

-- When working with many calculations, we may end up having lots and lots
-- of parenthesis for helping to execute some expressions before others, like the
-- following
plusPower x = (x + 2) ^ x
triplePlusPower x = (x + (3 * x)) ^ x
-- For cases like this we can take advantage of currying and one special operator
-- that has the lowest precedence of all and is right associative, meaning it's the
-- last to be applied and is grouped by the right. We are talking of the `$` operator,
-- you can think of it as something that will apply everything on right first. It can
-- be used like a function delay, where we evaluate the leftmost part only after the
-- rightmost, thus, removing the need of grouping parenthesis for dependant calculations
plusPower' x = (^ x) $ x + 2
-- Above we used sectioning, a technique to pre-apply parameters to infix functions.
-- When doing `(^ x)` it will return a function that expects a number to be powered
-- to x, and when doing `(x ^)` it will return a function that expects a number to
-- be take x to the power of it. The only special case for this is the `-` because
-- Haskell has a syntax sugar which converts `-4` to a negative number, the `-` acts
-- like an alias to `negate`, its a syntatic overloading, thus making the creating of
-- that takes 4 out of a number impossible by sectioning the `-` infix function.
-- For the minus case, we need to use the subtract function which expects two numbers
-- and acts exactly like the `-` operator, so `(-4)` gives us a number, while `(subtract 4)`
-- gives us a function that will take 4 out of something, be aware to not make the
-- of confunding `(4 -)` with the previous, because the latter will take a number
-- out of four, not the opposite.
triplePlusPower' x = (^ x) $ (x +) $ 3 * x
-- You may think that we haven't removed all the parenthesis, but that is because
-- we need to turn the infix functions into normal functions, so we apply them to
-- just one argument, thus returning the curried function which waits for the rest
-- of the parameters. This had to be done because `$` has the type of `(a -> b) -> a -> b`,
-- meaning that it receives a function first and its first parameter later, for further
-- applying that parameter to the function, hence getting the function result. If
-- you want to get rid of the parenthesis you can create/use the prefix versions for each
-- operator
pow = (^)
add = (+)
sub = (-)
mult = (*)
plusPower'' x = flip pow x $ x + 2
triplePlusPower'' x = flip pow x $ flip add x $ 3 * x
-- `flip` here is used to flip the function and turn the parameters around, e.g,
-- `(^) 1 2` is equal to `1 ^ 2` whilst `flip (^) 1 2` is equal to `2 ^ 1` because
-- we flipped the `(^)` function. In the code above you might think that we would need
-- parenthesis to create the flipped function before applying it, but that isn't
-- necessary because function application has the highest precedence of all and is
-- left associative, so the alpha equivalence `flip (^) 1 2 == ((flip (^)) 1) 2`
-- holds true

-- When working with Haskell you may think that its declarations are very limited
-- given that we have to provide only expressions for them, thus being limited from
-- contructing some crazy conditional code, variables, etc, as in other languages (don't
-- think that those functions with lots of nested if and other things are good).
-- For overcoming this limitation to of declaring pieces of code inside a outer function
-- without introducting a declaration visible for the whole module, Haskell provides
-- `let` expressions and `where` bindings, as the name states, `let` gives the
-- ability of introducing expressions, and `where` introduces declarations. Let can
-- be used wherever an expression is allowed, and where introduces declarations to
-- the surrounding context.
plusWithWhere x = plus x
    where plus x = x + 2
plusWithLet x = let plus x = x + 2
                in plus x

-- Exercises: A Head Code
-- 1 => 5
-- 2 => 25
-- 3 => 30
-- 4 => 6

val1 = x * 3 + y
    where x = 3
          y = 1000

val2 = x * 5
    where y = 10
          x = 10 * 5 + y

val3 = z / x + y
    where x = 7
          y = negate x
          z = y * 10

-- # Chapter exercises
-- Parenthesization - alpha equivalence
-- 1 => 2 + 2 * 3 - 1 => 2 + (2 * 3) - 1
-- 2 => (^) 10 $ 1 + 1 => 10 ^ (1 + 1)
-- 3 => 2 ^ 2 * 4 ^ 5 + 1 => ((2 ^ 2) * (4 ^ 5)) + 1

-- Equivalent expressions
-- 1 => is
-- 2 => isn't
-- 3 => isn't -> 400 - 37 = (-) 400 37, the order in infix function is left to right
-- 4 => isn't -> `div` is an integral division, while / is fractional
-- 5 => isn't -> wrong parenthisation, * has a higher precedence

-- More fun with functions
-- Try entering the next declarations in the REPL
z2 = 7
y2 = z2 + 8 -- 15
x2 = y2 ^ 2 -- 225
waxOn = x2 * 5 -- 1125
-- 1
-- 10 + waxOn = 1135
-- (+ 10) waxOn = 1135
-- (-) 15 waxOn = -1110
-- (-) waxOn 15 = 1110

-- 3 -> first the expansion occurs and then the reduction
-- triple waxOn => waxOn * 3 => (x * 5) * 3 => ((y ^ 2) * 5) * 3 =>
--  (((z + 8) ^ 2) * 5) * 3 => (((7 + 8) ^ 2) * 5) * 3 => ((15 ^ 2) * 5) * 3 =>
--  (225 * 5) * 3 => 1125 * 3 => 3375

-- 4
-- Differently from the REPL, where the order of declaration matters because it needs
-- to know what something means upfront the declaration of another thing that uses the
-- before mentioned, in source code files we don't need to order them (but is a good
-- practice) because the compiler will evaluate the whole file and do the necessary
-- work to coordinate the declarations.
waxOn' = x * 5
    where x = y ^ 2
          y = z ^ 2
          z = 7

waxOn'' = let x = y ^ 2
              y = z ^ 2
              z = 7
          in x * 5

-- Note that declarations in where and let doesn't clash with outer scopes because
-- they override those declarations.

-- 5
-- Here I used sectioning, thus taking advantage of currying and creating a function
-- that expects a number and multiplies it by 3
triple = (* 3)

-- 6
-- As the signature of waxOff is the same as triple, and it only call triple with the
-- exact same parameters order, we can say that they are alpha equivalent, hence
-- making it possible to define one as the alias for another
waxOff = triple

-- 7
-- waxOff 10 = 30
-- waxOff (-50) = -150
