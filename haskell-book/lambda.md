# All You Need is Lambda

## Intermission: Equivalence Exercises

1. λxy.xz == λmn.mz
2. λxy.xxy == λa.λb.aab
3. λxyz.zx == λtos.st

## Chapter exercises

- Combinators - abstractions that has no free variables
    1. λx.xxx -> yes
    2. λxy.zx -> no
    3. λxyz.xy(zx) -> yes
    4. λxyz.xy(zxy) -> yes
    5. λxy.xy(zxy) -> no

- Normal for or Diverge - if something diverges, it never ends apart of how many times it was applied
    1. λx.xxx - normal
    2. (λz.zz)(λy.yy) - diverge
    3. (λx.xxx)z - normal -> zzz - beta normal -> if beta normal doesn't diverge, it's normal

- Beta reduce - application process
    1. (λabc.cba)zz(λwv.w)
        ```
        [a := z] = (λbc.cbz)z(λwv.w)
        [b := z] = (λc.czz)(λwv.w)
        [c := λwv.w] = (λwv.w)zz
        [w := z] = (λv.z)z
        [v := z] = z
        ```
    2. (λx.λy.xyy)(λa.a)b
        ```
        [x := λa.a] = (λy.(λa.a)yy)b
        [y := b] = (λa.a)bb
        [a := b] = bb
        ```
    3. (λy.y)(λx.xx)(λz.zq)
        ```
        [y := λx.xx] = (λx.xx)(λz.zq)
        [x := λz.zq] = (λz.zq)(λz.zq)
        [z := λz.zq] = (λz.zq)q
        [z := q] = qq
        ```
    4. (λz.z)(λz.zz)(λz.zy)
        ```
        (λz.z)(λz.zz)(λz.zy) = (λz.z)(λa.aa)(λb.by) -> α equivalence
        [z := λa.aa] = (λa.aa)(λb.by)
        [a := λb.by] = (λb.by)(λb.by)
        [b := λb.by] = (λb.by)y
        [b := y] = yy
        ```
    5. (λx.λy.xyy)(λy.y)y
        ```
        (λx.λy.xyy)(λy.y)y = (λx.λy.xyy)(λa.a)b -> α equivalence
        [x := λa.a] = (λy.(λa.a)yy)b
        [y := b] = (λa.a)bb
        [a := b] = bb
        ```
    6. (λa.aa)(λb.ba)c
        ```
        (λa.aa)(λb.ba)c = (λa.aa)(λb.bm)c -> α equivalence
        [a := λb.bm] = (λb.bm)(λb.bm)c
        [b := λb.bm] = (λb.bm)mc
        [b := m] = mmc
        ```
    7. (λxyz.xz(yz))(λx.z)(λx.a)
        ```
        (λxyz.xz(yz))(λx.z)(λx.a) = (λxyz.xz(yz))(λa.b)(λm.n) -> α equivalence
        [x := λa.b] = (λyz.(λa.b)z(yz))(λm.n)
        [y := λm.n] = (λz.(λa.b)z((λm.n)z))
        [a := z] = (λz.b((λm.n)z))
        [m := z] = λz.bn
        ```