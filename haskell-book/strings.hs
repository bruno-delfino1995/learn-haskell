-- ## A first look at types
-- Strings are oftOen refered as lists of characters and in Haskell it couldn't be
-- different. They are represented through a text enclosed between double quotes,
-- e.g, `"your name"` (BTW an excellent movie). If you think that this creates a
-- special structure instead of a list of chars you can check by typing `:t "str"`
-- into GHCi, thus getting `"str" :: [Char]` as result, which can be read as "str"
-- has the type of an array/list of characters. For single characters you need to
-- use single quotes to enclose a single character, and by enclosing a bunch of
-- characters in square brackets, you get a list of characters, which when compared
-- to a String returns True if the values are the same.

aString = "str"
aListOfChars = ['s', 't', 'r']
isStringAListOfChars = aString == aListOfChars

-- By being just a type alias to a list of chars, we can use any function that operates
-- on a list, such as `++` for concatenating two lists, and `concat` for concatenating
-- more than two lists. If you look at the type of `concat` you'll see that it's
-- the following type signature `Foldable t => t [a] -> [a]`, meaning that it operates
-- on Foldables with lists containing 'a' and returns a list of 'a', however if you
-- ask GHC for info on String you'll see that it isn't a instance of Foldable, it's
-- just an alias to [Char], and there's the magic, lists are instances of Foldable,
-- thus, can have concat applied on them. With this we can note that GHC is able to
-- discover the underlying types of an type alias and make the type signatures match
-- based on the real type, instead of the alias.
onlyTwo = "hello " ++ "world!"
nStrings = concat ["hello", " ", "world", "!"]

-- ## Printing simple strings
-- One common requirement for basic terminal programs is to print something onto the
-- screen. In Haskell it's done by using `print`, however for `String` it'll output
-- the quotation marks, if you want to remove the quotation marks you should use
-- `putStr` which only removes the quotation or `putStrLn` which removes the quotation
-- but add a newline to the end.
withQuotation = print "Your Name" -- results in "Your Name" printed to the terminal
sameLine = do -- results in "YourName" given that we haven't put a space and also used `putStr`
    putStr "Your"
    putStr "Name"

-- For the basic "Hello World!" we would use `putStrLn` inside a main definition, given
-- that Haskell executes the `main` definition by default when running your applications.
-- If you want to execute it as a command line you'll need to compile, although
-- when using GHCi you can load your module with `:l filename.hs`, which will load
-- the module and append its name to the interpreter's prompt, resulting in `*ModuleName >`.
-- Whether you want to have a static prompt you can always execute `:set prompt "your prompt"`
-- or do the same in one of the files that GHCi loads by default, such as *~/.ghci*
main :: IO ()
main = putStrLn "Hello World!"

-- As you can see, main has the type of IO (), which in Haskell means that this
-- definition involves effects beyond evaluating the functions, such effects are
-- external sources of data, external outputs, any side-effect. This isn't noticeable
-- when running `putStrLn` in GHCi because it implicitly handles effects.
-- The syntax that you saw before, the `do` in `sameLine`, is a special syntax for
-- sequencing actions, e.g., printing the first string before the second; it has an
-- alternative, which makes the inner workings more explicit, but the alternative
-- is a little cumbersome

-- ## Top-level versus local definitions
-- All of the definitions we have created in this file are top level definitions,
-- meaning that they aren't nested within anything else, and are visible to the whole
-- file if not overwriten by a local definition. Local definition in the other hand
-- are nested within top-level definitions or under a upper local definition.
topLevelFunction :: Integer -> Integer
topLevelFunction x =
    x + localValue + topLevelValue
    where localValue :: Integer
          localValue = 10

topLevelValue :: Integer
topLevelValue = 5
-- Top level definitions can be exported, however local definitions introduced by
-- where bindings or let expression are only visible to their scope and can't be
-- exported once they don't belong to the module's top scope.

-- Exercises: Scope

-- 1. Yes, let expressions in GHCi are handled as top level definitions, so once you
--  have defined x and y they'll be available for the rest of the session

-- 2. No, h isn't defined yet

-- 3. No, d isn't defined to be used within r, we should use a local level definition
--  if we want to access the `d` which area receives as parameter
area d = pi * (r * r)
    where r = d / 2

-- 4. Yes, we are using a local definition for r which gives us access to d, and
--  as we are using where bindings then r becomes available for the expression in
--  area

-- ## Types of concatenation functions
-- As a String is just a list of chars, we get all the concatenation functions for
-- free, however, the main two `(++)` and `concat` have their differences, the first
-- has the type of `[a] -> [a] -> [a]`, meaning that it concatenates only two lists,
-- and the latter has a much broader definition, which is `[[a]] -> [a]`, thus representing
-- that it turns a list of lists into a flattened list. For querying type information
-- about something you can type `:t (++)` in GHCi
concatForMoreThanTwo = concat ["Hello", " ", "World", "!"]

appendForOnlyTwo = "Hello" + " World!"

-- As you saw, just by looking at the type signature we can tell a lot about the function
-- however you need to know how to read those signatures. Let's start with (++),
-- the first fragment of it shows that it receives a list of 'a', then another, and
-- finally returns a new one. This is the way how Haskell handles types polymorphism,
-- it declares the type with a type variable, hence removing the need to know it at
-- compile time and leaving that type to be defined at runtime. Although, remember
-- that lists in Haskell are homogenous, preventing you from mixing different types
-- of lists on concat, basically, once you define the first element of the list, you'll
-- have to use it. Another thing to be aware is that once the type variable is bound
-- to a specific type on a signature, the occurences of that variable will be replaced
-- by the concrete type being used.
elementsArePolymorphic = concat [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

-- Exercises: Syntax Errors
-- 1. Won't work, (++) is an infix operator, the correct code would be `[1, 2, 3] ++ [4, 5, 6]`

-- 2. Won't work, the Char type is meant to hold only one character, in this case we
-- should be using String - `"<3" ++ "Haskell"`

-- 3. Will work, concat operates on a list of lists, given that we are providing a list
-- of Strings, everything is fine

-- ## More list functions
-- Given, as forementioned, that String are just list of `Char`s we can use the `:`
-- operator to prepend to the list a new char, and all of the other basic list functions
someone = 'C' : "hris"
startsWith? = head "Chris"
afterInitial = tail "Chris"
onlyFirstTwo = take 2 "Chris"
afterFirstTwo = drop 2 "Chris"
withSurname = "Chris" ++ " " ++ "Rock"
atThirdPosition = "Chris" !! 2

-- However, note that some list functions are said to be unsafe, once they throw
-- exceptions on some cases, as when handling empty lists
-- > head ""
-- > tail ""
-- > "" !! 1
-- In fact, most of the list functions within the Prelude are unsafe.

-- Chapter Exercises

-- Reading Syntax
-- 1
-- a. yes
-- b. no, `++` is an infix function. [1, 2, 3] ++ [4, 5, 6]
-- c. yes
-- d. no, there's a double quote missing. ["hello" ++ " world"]
-- e. no, the argument's order is inverted. "hello" !! 4
-- f. yes, the left when using the infix goes first.
-- g. no, the number needs to be out of the string. take 4 "lovely"
-- h. yes

-- 2
-- a -> d
-- b -> c
-- c -> e
-- d -> a
-- e -> b

-- Building functions
-- 1
-- a. empasize = (++ "!")
-- b. atFive = (!! 4)
-- c. afterNine = drop 9

-- 2. empasize == sentence ++ "!"
--      where sentence = "Curry is Awesome"
--    atFive = sentence !! 4
--      where sentence = "Curry is Awesome"

-- 3. thirdLettler :: String -> Char
--    thirdLetter = (!! 2)

-- 4. letterIndex :: Int -> Char
--    letterIndex = ("Curry is Awesome" !!)

-- 5. rvrs :: String -> String
--    rvrs s = doRvrs s []
--        where doRvrs [] acc = acc
--              doRvrs str acc = doRvrs (drop 1 str) (take 1 str ++ acc)