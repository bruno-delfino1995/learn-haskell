import Control.Applicative
import Data.List
-- Applicative are a subclass of Functor that maps a wrapped function against
-- a wrapped value. The fact of being a subclass of Functor is a contraint in the
-- class declaration `class (Functor f) => Applicative f where ...`
listOfFuncs :: [[Char] -> [Char]] -- A wrapped function
listOfFuncs = fmap (:) "A LIST OF CHARS"

letterA :: [[Char]] -- A wrapped value
letterA = ["AB", "CD"]

-- The instance of Applicative for [] is basically applying each value of an array
-- to each function on the another. In this case appending "AB" and "CD" to each of
-- the elems because of the functions are [(:) 'A', (:) ' ', ...]. And this is the
-- normal behavior for Applicatives, they simply extract the function for one of
-- the boxes and the value from the another, and finally apply the value to the function
appended = listOfFuncs <*> letterA

-- Remind that a common synonym for functors are boxes that contain a value, but that
-- box, as in arrays, can contain more than one value, and the behavior for each applicative
-- depends on its instance. Another anology is 'computational contexts', the values
-- in an array are within the context of the array

-- By using Applicatives we can operate on several functors until the resulting
-- functor is a value and not a function, which is a limitation of functors because
-- they can't extract the function of the first parameter - in reality what blocks it
-- is the type definition of `fmap`
justSum = pure (+) <*> Just 5 <*> Just 8
nothingComesInNothingGoesOut = pure (+) <*> Just 5 <*> Nothing
-- You might be wondering how does `+` fits in the declaration of `<*>`, which is
-- `(<*>) :: f (a -> b) -> f a -> f b`. It fits due to currying \o/, you can consider
-- `b` as the curried function returned by `(+) :: a¹ -> (b -> c)²`, which matches to
-- `(<*>) :: f (a¹ -> b²) -> f a¹ -> f b²` wherein the f plays the role of the
-- Applicative

-- One of the rules for applicatives is that it can be replaced by a fmap with partial application.
justSumByMapping = fmap (+) (Just 5) <*> Just 8
-- To make that short we have `f <$> x` that is the same as `fmap f x`, its the infix version for fmap
justSumByMapping' = (+) <$> Just 5 <*> Just 8
-- That could be shortened to `(+) 5 8` if the parameters weren't in a Maybe context
-- If you want to understand better the above statement you can think of it as a
-- partial application of `+` in 5 because we mapped a function over a value, and then
-- we map the same function over another mapped value. When checking the middle steps
-- using type anotations we can see that. Here follow the steps
-- 0. (a -> a -> a) <$> Maybe Int <*> Maybe Int
--  taking that <$> fmap, and is left associative, we apply the first segment
--   fmap (a -> b)¹ -> f a² -> f b³ where
--    (a -> b)¹ = (a -> a -> a)
--    f a² = Maybe Int -- we get the ³ by applying the a of ² inside ¹
--       and since we applied one part of the function the resulting function (partially applied) gets returned
--       remember to keep the same shape because f is the same, also keep note
--       that the wrapped function could be anything but it follows the type of the
--       first parameter because the same type variable is used forth, if the definition
--       of `+` were `a -> b -> c` then we would have f b³ equals to `Maybe (b -> c)`
--    f b³ = Maybe (Int -> Int)
--  Now gluing this resolution we got
-- 1. Maybe (Int -> Int) <*> Maybe Int
--  here `<*>` is the applicative part which has the type definition as `f (a -> b) -> f a -> f b`
--  we couldn't use `fmap` again because it doesn't match the given types, it expects
--  a plain function, not a wrapped function, as the first parameter
--  f (a -> b)¹ -> f a² -> f b³ where
--   f (a -> b)¹ = Maybe (Int -> Int) -- f = Maybe, a = Int, b = Int
--   f a² = Maybe Int -- the types must match, otherwise we would have a compilation error
--   f b² = Maybe Int -- we applied `f a` to `f (a -> b)` and by matching the types and
--     pre applying the function we get `Int` as its result
-- 2. Maybe Int

-- Applicatives for Lists are just list comprehensions between the participants
-- where we do a cartesian product on the values. You can think of applying one list
-- of functions to one list of values a non-deterministic computation, because there's
-- no way for the algorithm to know which value you want, whilst a computation
-- of 1 + 2 is deterministic because we have only one possible result. Once applicatives
-- for lists are cartesian products we can use that to substitute list comprehensions
-- by them
listComp = [ x * y | x <- [1, 2, 3], y <- [4, 5, 6]]
-- Given that we want a computation over all of the values, and don't want to do that
-- by hand, we are going to map first, in order to pre apply the function, and then
-- apply those functions over the values
listAp = (*) <$> [1, 2, 3] <*> [4, 5, 6]
canIReplace = listComp == listAp

listComp' = [ x * y | x <- [1, 2, 3], y <- [4, 5, 6], x * y > 50]
listAp' = filter (> 50) $ (*) <$> [1, 2, 3] <*> [4, 5, 6]
canIReplace' = listAp' == listComp'

-- By the last two definitions it's easy to see how `pure f <*> xs` the same as
-- `fmap f xs`. But for that we have to understand that pure simply gets some value
-- and puts it into a default context, which for [] is [f] meaning the presence of a value
-- We couldn't have used an empty list because it would mean the lack of value, and
-- the same applies for Maybe, which returns a Just f when asked for a default context.
-- If we want a default context for a value we should at least have the value, that's
-- why we don't get Nothing or [] as result of pure for Maybe and [], respectively

-- One tricky instance of Applicatives is the one for IO actions, it's tricky because
-- we have to deal with sequencing the actions before doing the manipulations. Imagine
-- that you want to concatenate two lines, you need to get the first before getting the
-- second because we concatenate `"Hello" ++ "World"`, and when talking about IO
-- actions we have to remember that they will fetch a side effect before returning the value.
concatLines = (++) <$> getLine <*> getLine
-- If you from functors that mapping a function through an IO action returns the
-- mapped value, then you will realize that we partially apply (++) to the first line
-- to after concatenate it with the second line. But since we need to fetch the
-- first line first we execute unwrap the function first in the applicative. Otherwise
-- if the user types "Hello" and then "World" we would have "WorldHello", instead of
-- "HelloWorld".

-- Now here comes the always crazy instance of anything, the one for functions. The
-- instance of Applicative for ((->) r), aka function, is really confusing. It simply will
-- apply the value to each of the function however the first will get the function and
-- the value returned by the second. It applies a function returned by a function from `r`
-- (the first function, which if we partially apply simply returns a function) to a
-- value returned by a function from `r`. That can be seen after applying the type
-- to the signature of `<*>`, resulting in `(r -> (a -> b)) -> (r -> a) -> (r -> b)`.
-- (r -> (a -> b)¹) -> (r -> a²) -> (r -> b)³
-- (a -> b)¹ = a function returned by a function from r
-- a² = a value returned from a function from r
-- (r -> b)³ = the resulting composition, which specifies the `r` as its input
--  and by the signature you can see that this `r` will be applied to the first
--  and to the second function
apFunction = ((+) <*> (+5) $ 5) == 15
-- Given that the first function is (+) and the applicative applies the function returned
-- by a function from `r` <`5`> to the value returned by a function from `r` <`(+5) 5` = 10>
-- we get `15` as result. Here the term *function from r* simply means a function which
-- gets `r` as input and returns something. And a *function returned by a function from r*
-- means that we apply r and gets back another function, which is what happens with `(+)`
-- Following you can see the process of decomposing the type signature until we hit the value.
-- However we gonna use the code of ((->) r) instance to make it easier
-- f <*> g = \x -> f x (g x) - <*> for ((->) r)
-- (+) <*> (+5) = \x -> (+) x ((+5) x)
-- (+) <*> (+5) $ 5 = (\x -> (+) x ((+5) x)) 5
-- [5/x] in (\x -> (+) x ((+5) x)) 5 => (+) 5 ((+5) 5) = (+) 5 10 = 15

-- Another instance for applicatives that is useful is the ZipList, which is almost
-- like a alias to [] which basically apply the functions in a manner more like
-- zipping the sides.
zipWithFunc = zipWith (+) [1, 2, 3] [4, 5, 6]
zipWithAp = (+) <$> ZipList [1,2,3] <*> ZipList [4,5,6]
zipIsTheSame = zipWithFunc == (getZipList zipWithAp)
-- One usage that really makes the Applicative style shine is the partial application
-- of the function. Imagine that you want to zip 3 lists into a tuple, for that you
-- are covered with `zip3`, now think if you need to zip 4, 5, 6, n lists into a tuple
-- of size n. That wouldn't be possible once we don't have zip4 or zip5, however
-- giving that using applicatives we unwrap functions and apply them to values to maybe
-- get another function or a final value we can do whatever combinations until our
-- function is fully applied. Of course you could use `zipWith4 ... zipWith7` but what
-- happens if you need more that 7 lists. This also show a advantage of applicatives
-- over functors, which is that we can only map simple functions over functors, whilst
-- with applicatives you can map a wrapped function over a wrapped value, whichin the function
-- can be anything that matches the types - we can apply a function between several functors.
multZipFunc = zipWith5 (,,,,) [1, 2, 3] [4, 5, 6] [7, 8, 9] [10, 11, 12] [13, 14, 15]
multZipAp = (,,,,) <$> ZipList [1, 2, 3] <*> ZipList [4, 5, 6] <*> ZipList [7, 8, 9] <*> ZipList [10, 11, 12] <*> ZipList [13, 14, 15]
apShine = multZipFunc == (getZipList multZipAp)

-- One utility function that shrinks our map/pure part to create applicatives is the
-- `liftA`, which will lift a function to work with a given type of applicatives. Thus
-- removing the part of mapping one function over a functor, or creating a pure
-- applicative.
liftPlusToAp = liftA (+) (ZipList [1, 2, 3]) <*> (ZipList [4, 5, 6])
liftIsSameAsMap = liftPlusToAp == zipWithAp

-- Imagine that you want to concatenate values within a context and put them into
-- a new context after. This can be achieved by pre applying (:) to one functor
-- and them applying it to an applicative, as seen below.
justConcat = (:) <$> Just 5 <*> Just [6]
-- That is a common pattern of lifting one function and then applying it to another,
-- which is `liftA` among `<*>`. By being so common we have `liftA2` which simply does
-- that. It lifts the function to work on the given applicative and them apply it to
-- the next.
justConcat' = liftA2 (:) (Just 5) (Just [6])

-- Another common utility is to get a list of contexts, process those, and return a
-- list within a context. Which is simply a recursive way of doing the concatenation
-- shown above.
sequenceA' :: (Applicative f) => [f a] -> f [a]
sequenceA' [] = pure [] -- for an empty list we just put an empty list in the default context
sequenceA' (x:xs) = (:) <$> x <*> sequenceA' xs

-- Remember that when we go over a list accumulating elements, whether or not to generate 
-- a new list, we can use fold. In this case we need foldr otherwise the list would
-- be reverted
sequenceA'' :: (Applicative f) => [f a] -> f [a]
sequenceA'' = foldr (liftA2 (:)) (pure [])

-- Here we computate the contexts to see if there is value in all of them, if not
-- we return an empty context. This is done by the applicative/functor nature of Just,
-- 'if Nothing comes in Nothing goes out'
withoutNothing = sequenceA' [Just 1, Just 3, Just 5] == Just [1, 3, 5]
withNothing = sequenceA' [Just 1, Nothing, Just 2] == Nothing

-- Another cool use for this sequence function is to apply a single value to a bunch
-- of functions and get back an array of all the values
oneInputManyOutputs = sequenceA' [(+3),(+2),(+1)] 5
-- Below you can see the generated functions for this application, given that <$> is
-- function composition and that <*> simply creates a unary function that feeds both functions
-- and the first with the output of the second
first = (\x -> (\y -> (:) ((+3) y)) x (second x))
second = (\x -> (\y -> (:) ((+2) y)) x (third x))
third = (\x -> (\y -> (:) ((+1) y)) x ((pure []) x))

-- Now one crazy thing that we can do is use lists a list of lists to create every
-- possible combination of two items between them. That simply happens because if we
-- have a list within a list, the inner list would match with the sequence header and
-- start an inner sequence
permutationOfTwo = sequenceA' [[1,2],[3,4]]
-- Quote LYAH - Ch11 Functors, Applicative Functors and Monoids - Applicative functors
-- - We start off with sequenceA [[1,2],[3,4]]
-- - That evaluates to (:) <$> [1,2] <*> sequenceA [[3,4]]
-- - Evaluating the inner sequenceA further, we get (:) <$> [1,2] <*> ((:) <$> [3,4] <*> sequenceA [])
-- - We've reached the edge condition, so this is now (:) <$> [1,2] <*> ((:) <$> [3,4] <*> [[]])
-- - Now, we evaluate the (:) <$> [3,4] <*> [[]] part, which will use : with every
--      possible value in the left list (possible values are 3 and 4) with every
--      possible value on the right list (only possible value is []), which results
--      in [3:[], 4:[]], which is [[3],[4]]. So now we have (:) <$> [1,2] <*> [[3],[4]]
-- - Now, : is used with every possible value from the left list (1 and 2) with every
--      possible value in the right list ([3] and [4]), which results in [1:[3], 1:[4], 2:[3], 2:[4]],
--      which is [[1,3],[1,4],[2,3],[2,4]
