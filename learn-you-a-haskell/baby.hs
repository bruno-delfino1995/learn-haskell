-- Functions are defined using the <name> <space separated params> = <definition>
doubleMe :: (Num a) => a -> a
doubleMe x = x + x

-- Function call have the highest precedence in expressions, you call them by the name
-- and pass parameters separated by space like `div 90 10`.
-- `div 90 10 + div 10 10` is the same as `(div 90 10) + (div 10 10)`
-- Unlike other languages we don't use parentheses to call functions, the space after
-- the function is for function application
-- That kind of call is called prefix and when you use an operator like in `1 + 2`
-- we say that the + is in the infix form.
-- If you want to use a function in it's infix form you have to use backticks
-- on the name, by so we make the previous call to div much more explicit by declaring
-- which is the divisor and the dividend - `90 `div` 10`
doubleUs x y = doubleMe x + doubleMe y

-- One strange and at the same time great things of Haskell if is that it enforces
-- the else block, once if is an expression it must return a value no matter what
-- TODO: see if the expression part holds true or find a better explanation, because
--  the case throws an runtime error if a pattern hasn't matched with the value
doubleSmallerNumber x = if x > 100
    then x
    else doubleMe x

-- The apostrophe at the end of the function doesn't have any special meaning, but in
-- Haskell it's normally used for functions that aren't lazy
doubleSmallerNumber' x = (if x > 100 then x else doubleMe x) + 1

-- When the function doesn't take any parameter then we say that it's a definition (or a name)
-- because we can't change its meaning once we've defined it
conanO'Brien :: String -- Types annotations can be specified for any definition, the
-- `::` means 'has type of', like here *conanO'Brien* **has type of** *String*
conanO'Brien = "It's a-me, Conan O'Brien!"