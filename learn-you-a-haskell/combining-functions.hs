-- We well know that function application is done by ` ` (space) and that it's
-- left-associative, meaning that `(((f a) b) c) == f a b c`, and has a high
-- precedence, it gets applied first. So when we write `sum (map sqrt [1..])` we
-- have to use the parenthesis to pass the result of `map sqrt [1..]` instead of
-- passing them all as parameters. In order to stop using the parenthesis we have the
-- `$` operator that is simply function application but [right-associative](https://stackoverflow.com/questions/930486/what-is-associativity-of-operators-and-why-is-it-important)
-- so `sum $ map sqrt [1..]` is the same as before, has its name, it acts like an operator,
-- given that it has the lowest precedence it would leave the evaluation of
-- `sum` and `map sqrt [1..]` to its own and apply the result to each other. When using `$`
-- the value on its right is evaluated then passed to the left, it's like an
-- inverted pipeline operator
appliedFuncs = map ($ 3) [(4 +), (10 *), (^ 2), sqrt]

-- Here we define the pipeline operator as being the inverse of normal function application
(|>) :: a -> (a -> b) -> b
x |> f = f $ x -- The same as `x |> f = f x`

(<|) :: (a -> b) -> a -> b
f <| x = f $ x

-- Another cool facility is the function composition operator `.`, in math defined as
-- `(f . g)(x) = f(g(x))` and in Haskell `(.) :: (b -> c) -> (a -> b) -> a -> c`.
-- It can be used to remove those long chains of parenthesis and create point free
-- functions. Remember the `oddSquares` written in the `high-order-function.hs`, it
-- could have been writeen as
oddSquares = sum . takeWhile (<= 10000) . filter odd . map (^ 2) $ [1..]
-- Here the trick is to remove all the parenthesis and use compositions and on the
-- inner most function you left the last parameter for later application using the
-- `$` operator. As you have guessed by its definition the `.` operator accepts only
-- functions that takes only one parameter so you can use currying to pre-apply the
-- arguments as done in the example above. Haskell has more a mathematician approach
-- to problem where you compose functions and apply them instead of writing data-flows
-- as in Elixir, that would look likes this
oddSquares' = [1..]
    |> map (^ 2)
    |> filter odd
    |> takeWhile (<= 10000)
    |> sum

-- One counter side for the Elixir approach is that we couldn't benefit from currying
-- and writing a 'point free style' we would have to repeat the parameter, if existent,
-- in the beginning of the flow, so we'd have a little duplication
-- Initial func
fn x = ceiling (negate (tan (cos (max 50 x))))

-- Haskell with composition
fnH x = ceiling . negate . tan . cos . max 50 $ x
-- Haskell point free
fnH' = ceiling . negate . tan . cos . max 50

-- Elixir data flow
fnE x = x |> max 50 |> cos |> tan |> negate |> ceiling
-- Although while writing this I've realized that we could benefit from currying and
-- write in a point free style, we just have to pre-apply one of the sides of `|>`,
-- but I don't know how to do it yet
-- TODO: find a way to resolve this by pre-applying the `|>` operator, e.g.
-- `fnE' = (|> max 50 |> cos |> tan |> negate |> ceiling)`