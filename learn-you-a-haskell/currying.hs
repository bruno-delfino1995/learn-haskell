-- Haskell by default creates auto curryied functions, which mean that until
-- all parameters have been provided, the called function will return another
-- function for taking the rest of those parameters. By default, and according
-- to Lambda Calculus Definition (find link), each function takes only one parameter
-- and returns a value, that can be another function or a value.
-- When calling function we say that the space is for function application, so when you
-- call a function with two parameters, you actually is applying a value to two different
-- functions, check this `max 4 5 == (max 4) 5` in the first space we apply 4 to max,
-- as max receives two parameters and Haskell autocurries it, we get another function
-- that will wait for the next parameter and return a value.
-- This is the reason of why on type definitions we separate parameters by `->`,
-- the `->` means takes this and returns this, by so we understood that only the last
-- member of the type declaration is our return value

-- The declaration below is the same as `multThree :: (Num a) => a -> (a -> (a -> a))`
multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z

-- By this property we can make really awesome thing, like partially applying function for
-- further use - [check this](https://hughfdjackson.com/javascript/why-curry-helps/)
multByFifteen :: (Num a) => a -> a
multByFifteen = multThree 3 5

-- Another cool thing is that we can partial apply operators also, we simply gets its function
-- form by encapsuling it in parenthesis (no applied params -> `(+)`) and if we want
-- to apply parameters we provide them for one of the sides. The only trick is with the
-- `-` operator, for convinience `(- 4)` is `-4` not `x - 4`, to work around this we can
-- provide the 4 in the other side of use the subtract function -> `(4 -)` or (subtract 4)
minus4 = (-4)
minusPartialWith4 = (4 -)
-- But remember that here you are taking something out of 4, not taking 4 out of
-- something - `(4 -) 10 == 4 - 10` and not `(4 -) 10 == 10 - 4`
-- Let's say you want to take 4 out of 10 twice, check the example:
--   `(4 -) ((4 -) 10) == 10` -> 4 - 10 = -6 -> 4 - (-6) = 10
--   `(subtract 4) ((subtract 4) 10)` -> 10 - 4 = 6 -> 6 - 4 = 2
-- So to skip from mind twists, try to not use this kind of partial application,
-- when partially applying to infix functions (aka. operators), always apply to the
-- right side of the operator, and remember that for `-` you have to use `subtract`
subtract4 = (subtract 4)

minusCurrying = (minus4 == minusPartialWith4 8) == (minus4 == subtract4 8)

compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare 100 x

-- This has the same type annotation as the above because compare applied once waits
-- for a (Num && Ord) and returns an Ordering, behind the scenes `compare 10` returns
-- a single parameter function that returns Ordering when called
compareWithTen :: (Num a, Ord a) => a -> Ordering
compareWithTen = compare 10

isUpperAlphaNum :: Char -> Bool
isUpperAlphaNum = (`elem` ['A'..'Z'])

-- High order functions are functions that take functions as parameters or return functions.
-- As each Haskell function is autocurried we can say that until it is fully applied,
-- every Haskell function is a high order fuction because all of them return a new
-- function with the rest of the parameters chain.

-- If you want to specify that you are expecting functions as parameters you have to
-- enclose it by parenthesis with its signature. `applyTwice` receives a function that
-- given `a` resolves to `a`, it also expects a value to apply to that function
applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)

-- If you want a function that takes more than just one parameter, just specify that
-- between the parenthesis :D. Remember that when using the same type variables you mean
-- that those parameters/returns have to be of the same type, but when using different
-- type variables, you let the caller use different or same types. You can receive
-- `a`, `b` and `c` all as Integers, or `a` and `b` as Char and in the function to generate
-- `c` you specify that its return is of type Integer, then you gonna have a & b = Char and c = Integer
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ _ [] = []
zipWith' _ [] _ = []
zipWith' f (x:xs) (y:ys) = ((f x y):zipWith' f xs ys)

-- Flip is defined in the standard library, it simply make the first parameter be the second,
-- and the second the first
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = g
    where g x y = f y x

-- By the type declaration of flip we can see that it receives a function that takes
-- a and b, and turns it into a function that takes b and a, with each of them separated
-- by parenthesis. Although as `->` is right associative, the second pair of parenthesis
-- is unnecessary, e. g. `(a -> b -> c) -> (b -> a -> c)` is the same as `(a -> b -> c) -> (b -> (a -> c))`
-- Here we can take advantage of currying, we just call `flip'' f` and it will return
-- a curried function that take the next two parameters and flip them, if there are more
-- parameters remaining they will be taken by the curried version after fully applying flip.
-- Example:
--  flip'' zipWith [2, 2..] div [10, 8, 6, 4, 2] == zipWith div [2, 2..] [10, 8, 6, 4, 2]
-- Although how the type declaration of zipWith -  `zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]`
-- fits `(a -> b -> c)` I don't know
flip'' :: (a -> b -> c) -> b -> a -> c
flip'' f y x = f x y

-- One interesting 'notation' when talking about currying, is that if the function to
-- be curried takes another function and do something with it, and on top of that
-- we use curry to partially apply the function then we can say that we have lifted
-- the function, because it will have its implementation composed within the
-- high-order-function - we are lifting its functionallity to work with something more specific
-- Example: `fmap` takes a function as first parameter (`(a -> b) -> f a -> f b`),
-- when reading its type declaration we can say that it takes a function and a functor
-- resulting in a new functor, using the new 'notation' we say that it lifts a function
-- so that can operate on functors
-- Although this isn't true for every currying which we do, this is a specific notation
-- for high-order-functions that take functions as parameters, and once that in Haskell
-- we pass the subject of the function as the last argument, normally it will turn
-- that functions would end in the beginning of the parameters