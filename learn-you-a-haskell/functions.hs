-- Like in Elixir, Haskell as pattern matching as well, it's identical to Elixir
-- as far as I know, the patterns match top to bottom and the first that matches
-- will get executed
lucky :: (Integral a) => a -> String -- Int and Integer are part of Integral typeclass
lucky 7 = "LUCKY NUMBER SEVEN!"
lucky x = "Sorry, you're out of luck, pal!"

factorial :: (Integral a) => a -> a
factorial n = product [1..n]

-- When specifying patterns, it's best to specify the most specific first and then
-- a catch all clausule. Whether we had specified the second first the factorial calculus
-- would never stop
factorial' :: (Integral a) => a -> a
factorial' 0 = 1
factorial' n = n * factorial (n - 1)

addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors a b = (fst a + fst b, snd a + snd b)

addVectors' :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors' (ax, ay) (bx, by) = (ax + bx, ay + by)

tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
-- When using the `:` (cons operator) we have to delimit the pattern by parenthesis
tell (x:[]) = "The list has one element: " ++ show x -- Once `x::[]` is just the
-- desugared version of `[x]` we could use define the patter as `tell [x]`
tell [x, y] = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "This list is long. The first tow elements are: " ++ show x ++ " and " ++ show y

length' :: (Num b) => [a] -> b
length' [] = 0
-- We can use `_` to specify that we don't care about that value, and it wouldn't even
-- be bound
length' (_:xs) = 1 + length' xs

-- We can capture the parameter itself before descructuring on pattern matching by
-- adding a name and an `@` before the pattern. We use it to avoid repeating ourselves
-- like in the function below, we could easily use `x:xs` to represent the whole string
-- but we added a `pattern` before it to capture the whole thing
capital :: String -> String
capital "" = "Empty string, whoops!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

-- Haskell also has guard expressions, wheter patterns are for shape verifications, guards are
-- for boolean checks. They are delimited by a pipe `|` and a bollean expression before the
-- function body, usually they are written in separated lines, altought they can be written in the same.
-- When checking for the appropriated function body, Haskell will fall through the
-- guards until it finds a expression resulting in True, if it haven't found tell it
-- will fall to the next pattern match - next function head
bmiTell :: (RealFloat a) => a -> String
bmiTell bmi -- when using guards we define the function body (using =) only after each of the guards
    | bmi <= 18.5 = "You're underweight, you emo, you!"
    | bmi <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | bmi <= 30.0 = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!" -- otherwise is defined as `otherwise = True`

describeMe :: (RealFloat a) => a -> a -> String
describeMe weight height
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | weight / height ^ 2 <= 30.0 = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"

-- See that in the upper definition we have repeated ourselves three times by calculating
-- the bmi on each guard, we can get rid of that kind of repetition by using where
-- in our functions. The `where` will simply define names or function that would
-- span all over the guards
describeMe' :: (RealFloat a) => a -> a -> String
describeMe' weight height
    | bmi <= skinny = "You're underweight, you emo, you!"
    | bmi <= normal = "You're supposedly normal. Pffft, I bet you're ugly!"
    | bmi <= fat = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"
    -- We can also use where to define constants to be used in our guards, improving
    -- readability, but remember that you must place the multiple definitions properly
    -- aligned in the same column
    where bmi = weight / height ^ 2
          skinny = 18.5
          normal = 25.0
          fat = 30.0

-- Once where is just a spot to define things, we can use it to define functions
-- to be used in our function's body as well
calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi w h | (w, h) <- xs]
    where bmi w h = w / (h ^ 2)

-- `Where` clauses can also be nested, we can define helpers for our guards/bodies
-- and helpers for those helpers, just remember that indentation is really sensitive
-- when dealing with `where` clauses
calcBmis' :: (RealFloat a) => [(a, a)] -> [a]
calcBmis' xs = [bmi w h | (w, h) <- xs]
    where bmi w h = w / power2 h
            where power2 x = x * x

-- Let bindings are very similar to where bindings, although where bindings are
-- syntatic contructions (doesn't return a value) and let bindings are expressions.
-- Another difference is that where can span the values all across the guards and
-- bodies, and let bindings are contrained to its own body after the `in`.
-- They shape is `let <bindings> in <expression>`, like where we can use pattern
-- matching on those bindings and they have to be aligned in the same column, and
-- for the expression we get those values/functions available for use
cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^ 2
    in sideArea + 2 * topArea


-- Here is the proof that let bindings are expressions, thus returning a value
-- computed by its body
expr :: Integer
expr = 4 * (let a = 9 in a + 1) + 2

-- For guards we can inline them using the pipe before each guard, but for bindings
-- in `let` we inline them by using a semicolon as separator, you can also put a
-- semicolon after the last bind if desired
expr' :: (Integer, String)
expr' = (let a = 100; b = 200; c = 300 in a * b * c, let foo = "Hey "; bar = "there"; in foo ++ bar)

-- You can also use let bindings inside list comprehensions, but you defined them
-- separated from the output function, and don't specify an expression for it
calcBmis'' :: (RealFloat a) => [(a, a)] -> [a]
calcBmis'' xs = [bmi | (w, h) <- xs, let power2 x = x * x; bmi = w / power2 h]

-- If you want to use a predicate together with a let binding you just have to separate
-- them by a comma
findFatties :: (RealFloat a) => [(a, a)] -> [(a, a, a)]
findFatties xs = [(w, h, bmi) | (w, h) <- xs, let power2 x = x * x; bmi = w / power2 h, bmi >= 25.0]

-- We don't specify the expression because that the visibility of the names is implied,
-- but we can use the in expression to specify a predicate function and keeping the
-- names constrained to that
findFatties' :: (RealFloat a) => [(a, a)] -> [(a, a)]
findFatties' xs = [(w, h) | (w, h) <- xs, let power2 x = x * x; bmi = w / power2 h in bmi >= 25.0]

-- Another useful thing for let bindings is that it keeps the names constrained to the
-- expression, like if we try `4 * (let a = 9 in a + 1) + 2 + a` would yield us an error
-- telling that a isn't in the scope. Another useful thing is when in GHCi you can use
-- let bindings without the `in` part to leak those bindings to the scope

-- We have been using pattern matching on function parameters so far, but how can we
-- use that inside a function body. Here it comes the `case` expression, pattern
-- matching on function parameters are just syntatic sugar for case expressions.
-- Case expressions follow the same dynamics as pattern matching on parameters,
-- they match a expression against a list of patterns and the fist that matches successfully
-- gets executed, and when no pattern is found it throws a runtime error
describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of [] -> "empty"
                                               [x] -> "a singleton list"
                                               xs -> "a longer list"
                                ++ "."

-- Case expressions are useful when pattern matching something in the middle of another
-- expression, but that polutes the code a bit. Because pattern matching on parameters,
-- or where bindings are just syntatic sugar for case expression we can choose between them
-- instead of using those polutting case expression
describeList' :: [a] -> String
describeList' xs = "The list is " ++ what xs ++ "."
    where what [] = "empty list"
          what [x] = "singleton list"
          what xs = "a longer list"