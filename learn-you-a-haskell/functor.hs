import System.IO
-- Functor is a type class for mapping over things. You can think of it as a box
-- with a value that will be unboxed, mapped, and boxed again. This box analogy
-- is for easiness on understanding but the real meaning of the Functor is a *computation
-- with context*, e.g.:
-- - Having or not a value (Maybe)
-- - Errored computations (Either)
-- - Multiple values ([])
-- - Side effects to the world (IO)
-- - Pending computation or Eventual result ((->) r) - Functor instance for functions

-- One common usage of the Functor is to map over an IO action before bounding its
-- result to a name. It's an clearer way of unwrapping, mapping, wrapping, although
-- in this case we would like to unwrap it to bound the result to a name. For that we
-- use the `<-` since unwrapping isn't the responsibility of Functors, this is one
-- question that easily comes around, the unwrapping of the functor can be done
-- by using specific operators/functions or pattern matching
main = do line <- fmap reverse getLine
          putStrLn $ "You typed " ++ line

-- One really mind blowing instance of functor is the function instance - by the way
-- functions can be represented as (a -> b) - which will map a function over a function
-- generating a new function, reminding you of function composition. So the `fmap` is
-- the same as `.`, but you have to remember that `.` gets the output from one and apply to another
-- and another and so on, however the parameters to generate the output of the first
-- function are applied in the end only. This is represented in the functor instance
-- by partially applying the first parameter of the function type, creating an instance
-- for ((->) r), making its kind match the one required by the Functor typeclass.
-- Remind that the `fmap` definition is `(a -> b) -> f a -> f b` now lets substitute the
-- f with the given type `(a -> b) -> (-> r) a -> (-> r) b`, given that `->` receives
-- two arguments and Haskell curries it we can shorten that declaration by using the
-- infix syntax `(a -> b) -> (r -> b) -> (r -> b)`, now using the right associativeness
-- of `->` we can shorten it more to `(a -> b) -> (r -> b) -> r -> b` which resembles
-- exactly as the `.` type (`(b -> c) -> (a -> b) -> a -> c`).
-- You can check that both are the same by doing `(.) (* 2) (+ 3) 4` `fmap (* 2) (+ 3) 4`
-- using the following Functor instance
-- ```
-- instance Functor ((->) r) where
--    fmap f g = (\x -> f (g x))
-- ```
functionFunctorIsComposition = (.) (* 2) (+ 3) 4 == fmap (* 2) (+ 3) 4
fmap' f g = (\x -> f (g x))

-- When implementing Functor or whatever other typeclass we have to make sure we follow
-- the type rules, this way following the Liskov Principle. For Functor we have mainly
-- two rules, which are:
-- 1. *Identity*: The result of mapping `id` over a functor should be the same as the original functor
--  - `fmap id = id` -> `fmap id (Just 5) == Just 5`
-- 2. *Composition*: Mapping a composition of two functions over a functor should be
-- the same as mapping the first over the functor and mapping the second over the result
-- of the first map
--  - `fmap (f . g) = fmap f . fmap g` -> `fmap (f . g) Nothing == fmap f (fmap g Nothing)`
-- These laws are a great thing to have because we can reason about the behavior of
-- a function easily, if the instance obeys the laws we can make assumptions and optimize
-- the code based on those. That way leading a more abstract and extensible code,
-- responsible only for mapping over the functor, responsibility that you can assure
-- by checking if the instance holds true when those laws are applied.
-- Here follow a functor impostor
data CMaybe a = CNothing | CMaybe Int a deriving (Show, Eq)
instance Functor CMaybe where
    fmap f CNothing = CNothing
    fmap f (CMaybe counter x) = CMaybe (counter + 1) (f x)

maybeCounter = CMaybe 0 "a"
firstLaw = fmap id maybeCounter == id maybeCounter
-- Normally when you break the first Functor's law you automatically breaks the second
-- because the first is like a side-effect checker
secondLaw = (f <$> (g <$> maybeCounter)) == ((f . g) <$> maybeCounter)
    where f = (++ "c")
          g = (++ "b")