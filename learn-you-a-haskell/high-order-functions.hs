-- As said in `currying.hs` High Order Function are functions that take or return
-- functions. The most common HOF are `map` and `filter`
pluThree = map (+3) [1,5,3,1,6]

odds = filter odd [1..]

-- Find the sum of all odd squares that are smaller than 10,000
squares = map (^ 2) [1..]
oddSquares = filter odd squares
oddSquaresLessThanTenThousand = takeWhile (<= 10000) oddSquares
sumOfAll = sum oddSquaresLessThanTenThousand
-- Can be writen as
result = sum (takeWhile (<= 10000) (filter odd (map (^ 2) [1..])))

-- Collatz sequences
chain :: (Integral a) => a -> [a]
chain 1 = [1]
chain n
    | even n = n : chain (n `div` 2)
    | odd n = n : chain (n * 3 + 1)

-- For all starting numbers between 1 and 100, how many chains have a length greater than 15?
numLongChains :: Int
numLongChains = length (filter isLong (map chain [1..100]))
    where isLong xs = length xs > 15

-- We can also create partially applied functions using map
listOfFuns :: (Integral a) => [a -> a]
listOfFuns = map (*) [1..]

twentyFive = (listOfFuns !! 4) 5

-- As any other functional programming language Haskell also has lambdas, they are
-- denoted by `\` followed by parameters separated by space then its body after `->`
-- they can be used to defined inline anonymous functions when necessary, we can pattern
-- match agains its parameters, have guards and everything else, however they can only
-- have one head. That means that we can use only one pattern, we can't have lets say
-- a pattern for [] and another for [(x:xs)]. Another point to mention is that normally
-- we use parenthesis to delimit them, otherwise it would span all the way to right, and also
-- for readability
sumPoints :: (Num a) => [a]
sumPoints = map (\(x, y) -> x + y) [(1,2),(3,5),(6,3),(2,6),(2,5)]

-- Another useful thing to do with lambdas is to make explicit that your function is
-- mainly meant to be partially applied and passed on to another function as parameter
-- or to get all its parameters applied, one example is the flip function, it's mainly
-- used to create another function with its parameter flipped, so we could write it as
flip' :: (a -> b -> c) -> b -> a -> c
flip' f = \x y -> f y x
-- That way we make explicit that we want to benefit from currying

sum' :: (Num a) => [a] -> a
sum' xs = foldl (\acc x -> acc + x) 0 xs
-- By taking into account that functions are curried we could write
-- sum' = foldl (+) 0
-- Generally, if you have a function like `foo a = bar b a` you could rewrite it
-- as `foo = bar b`

reverse' :: [a] -> [a]
reverse' = foldl (flip (:)) []
-- reverse' = foldl (\acc x -> x : acc) []

-- How many elements does it take for the sum of the roots of all natural numbers to exceed 1000?
sqrtSums :: Int
squareRoots = map sqrt [1..]
sumOfSqrt = scanl1 (+) squareRoots
sumsLessThanThousand = takeWhile (< 1000) sumOfSqrt
sqrtSums = length sumsLessThanThousand + 1