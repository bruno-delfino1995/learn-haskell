import Data.Monoid
-- Monoids are types designed for when we have an associative binary function
-- and a value which can act as an identity, e.g., * with 1, ++ with [], + with 0, ...
-- We can extract cuch type classes from common behaviors, the monoid is designed
-- when we have this properties:
-- - The function takes two parameters
-- - The parameters and return value share the same type
-- - There is a value which if applied to any side of the function doesn't change the other
-- By analyzing the third property we identify the associativity rule `1 + (1 + 2) == (1 + 1) + 2`,
-- and the identity `1 + 0 = 1`
-- Below you can see the typeclass for Monoid, defined in Data.Monoid
-- ```haskell
-- class Monoid m where
--  mempty :: m
--  mappend :: m -> m -> m
--  mconcat :: [m] -> m
--  mconcat = foldr mappend mempty
--```
-- If you take a look at it, you can see our identity value defined as a polymorphic
-- constant in `mempty`, the binary function as `mappend`, and latter an utility
-- for reducing a bunch of files to a single.
isListMonoid = [1,2,3] `mappend` [4,5,6] == [1,2,3,4,5,6]
emptyIsIdentity = mempty :: [a]
concatIsReduce = mconcat list == foldl (++) [] list
    where list = [[1,2],[3,4],[4,5]]

-- We've seen that numbers can act like monoids when using the operations `*` and `+`,
-- given that we have `Product` and `Sum` respectively. One thing that might seem
-- confusing when working with typeclasses is that the instance aren't made only
-- by the type, normally the criteria to create a new instance of a specific typeclass
-- can involve combining a operation with that type, like Sum and Product, we have
-- combined + and * with the type Num. Most of the times, those combinations must
-- comply to some restriction (rules) as well, like associativity, identitiy, composition, ...
foldWithPlus = foldl1 (+) [1,2,3]
foldWithSum = getSum . mconcat $ Sum <$> [1,2,3]

-- Other instances are these for `Bool`, `All` having the binary function as `&&` with
-- `True` as identity, and `Any` with `||` along `False`.
foldWithOr = foldl1 (||) [True, False, True]
foldWithAny = getAny . mconcat $ Any <$> [True, False, True]

-- A useful but confusing instance of Monoid is the Ordering, where the identity is
-- EQ, once anything compared to EQ is anything, and the mappend function is just
-- the first parameter if it isn't EQ. That can be useful when we have cascading
-- condition where we follow up only if the last is EQ, otherwise we return the last.
-- One useful case is for when you want to save time on string comparisons, you want
-- to full compare them only if the length is equal, otherwise return the length comparison
lengthCompare :: String -> String -> Ordering
lengthCompare x y = (length x `compare` length y) `mappend` (x `compare` y)

-- Once we have this cascading behavior we can use that to put more conditions before
-- the actual wanted condition. How about comparing the amount of vowels before the
-- full comparison?
lengthVowelCompare x y = (length x `compare` length y) `mappend`
                         (vowels x `compare` vowels y) `mappend`
                         (x `compare` y)
    where vowels = length . filter (`elem` "aeiou")

-- Have you noticed how good are monoid for making combinations? This is one property
-- which was taken advantage by using on folds, because folds are basically a combination
-- of all the items in a given structure, resulting in one single value. We can witness
-- that in `foldMap`, the minimal method to implement when making an intance of `Foldable`.
-- `foldMap :: (Monoid m, Foldable t) => (a -> m) -> t a -> m
data Tree a = Empty | Node a (Tree a) (Tree a)
instance Foldable Tree where
    foldMap f Empty = mempty
    foldMap f (Node x l r) = foldMap f l `mappend`
                             f x `mappend`
                             foldMap f r

testTree = Node 5
    (Node 3
        (Node 1 Empty Empty)
        (Node 6 Empty Empty)
    )
    (Node 9
        (Node 8 Empty Empty)
        (Node 10 Empty Empty)
    )

-- With folding and the combinatory nature of monoids we can do really cool things
-- Tranforming into a list (lists are monoids)
treeAsList = foldMap (\x -> [x]) testTree
-- Reducing the tree to a sum of all of them
sumTree = foldMap (Sum) testTree
-- Check if all are greater than 15
allGreater = foldMap (All . (> 15)) testTree
-- Check if any is even
anyEven = foldMap (Any . even) testTree