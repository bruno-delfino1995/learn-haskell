-- Randomness as IO is another source of side-effects, although Haskell has modeled
-- it in a manner where we can make the random side pure by using `mkStdGen :: Int -> StdGen`
-- which creates a generator for `System.Random.random`, making the randomization part
-- pure, e.g. `random (mkStdGen 100) == random (mkStdGen 100)`. However we have to choose
-- a number to pass in `mkStdGen` and for true ramdomness it must be random also,
-- for choosing that number - here comes the side-effect - we can use system information,
-- which is used by the system generator when we get it using `getStdGen`. By using
-- the system generator we have introduced a side-effect making it necessary to be
-- inside of an IO action.

import System.Random
import Control.Monad(when)

main = do
    gen <- getStdGen
    askForNumber gen

askForNumber :: StdGen -> IO ()
askForNumber gen = do
    let (number, nextGen) = randomR (1, 10) gen :: (Int, StdGen)
    putStrLn "Which number in the range of 1 to 10 am I thinking of?"
    guess <- getLine
    when (not $ null guess) $ do
        let numberGuess = read guess
        if numberGuess == number
            then putStrLn "You are correct"
            else putStrLn $ "Sorry, it was " ++ show number
        askForNumber nextGen

-- In the previous function you can see that we use the generator returned by randomR
-- for the next generation process, this is done in order to make the process truly random
-- otherwise we would be using the same generator x times, creating the same number
-- x times.
-- Now lets think in a scenario where we can't get the returned generator, I think
-- this will never happen, but lets continue, to overcome this we need to create a
-- new system generator and override the current by using `newStdGen` - after calling
-- this function the system generator will be overwriten making the next call to
-- `getStdGen` return a different generator from the previous call. Here follows
-- an alternative implementation with the own main recursive
main' = do
    gen <- getStdGen
    let (randNumber, _) = randomR (1,10) gen :: (Int, StdGen)
    putStr "Which number in the range from 1 to 10 am I thinking of? "
    numberString <- getLine
    when (not $ null numberString) $ do
        let number = read numberString
        if randNumber == number
            then putStrLn "You are correct!"
            else putStrLn $ "Sorry, it was " ++ show randNumber
        newStdGen
        main'