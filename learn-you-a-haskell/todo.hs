import System.IO
import System.Directory
import System.Environment
import Data.List

dispatch :: [(String, [String] -> IO ())]
dispatch = [("add", add),
            ("remove", remove),
            ("view", view)
           ]

add :: [String] -> IO ()
add [fileName, todoItem] = appendFile fileName (todoItem ++ "\n")

remove :: [String] -> IO ()
remove [fileName, n] = do
    handle <- openFile fileName ReadMode
    (tempName, tempHandle) <- openTempFile "." "temp"
    contents <- hGetContents handle
    let taskNumber = read n
        tasks = lines contents
        task = tasks !! (taskNumber - 1)
        newTasks = filter (not . (== task)) tasks
    hPutStr tempHandle $ unlines newTasks
    hClose handle
    hClose tempHandle
    removeFile fileName
    renameFile tempName fileName

view :: [String] -> IO ()
view [fileName] = withFile fileName ReadMode (\handle -> do
    contents <- hGetContents handle
    let tasks = lines contents
        numberedTasks = map (join " - ") . map (\(st, nd) -> (show st, nd)) . number 1 $ tasks
    putStr $ unlines numberedTasks)

join :: String -> (String, String) -> String
join glue (x, y) = x ++ glue ++ y

number :: Integer -> [a] -> [(Integer, a)]
number x = zip [x..]

main = do
    (command:args) <- getArgs
    let (Just action) = lookup command dispatch
    action args