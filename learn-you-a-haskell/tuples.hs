-- Tuples are like lists, they are containers for data. We should use then when we
-- know in advance how many elements the data should have, it's used for fixed length
-- elements, in the other hand arrays/lists are used for variable lengths. Also, tuples
-- allow us to use different types, they aren't homogenous like lists, they are
-- categorized/typed by its containing data, e. q. `("Xablau", 1)` isn't the same as `(1, "Xablau")`
person = ("Bruno", "Delfino", 1995)

-- As tuples are categorized by its types and lists are homogenous, we can't use different
-- size or type tuples inside a list, because we must have List[Integer] or List[Tuple(Int, Int)]
-- we can't change the type inside the list
-- [(1, 2), (1, 2, 3), (1, 2)] -- doesn't work because Tuple(Int, Int, Int) isn't Tuple(Int, Int)
square = [(0, 0), (0, 1), (1, 1), (0, 1)]

-- Here's an example using all we've seen so far
triangles = [(a, b, c) | a <- [1..10], b <- [1..10], c <- [1..10]]
rightTriangles = [(a, b, c) | c <- [1..10], b <- [1..c], a <- [1..b], a ^ 2 + b ^ 2 == c ^ 2]