module Types
(
Point(..) -- By doing Type(..) we have declared that we are exporting the type
-- and all its value constructors, by doing so, we let the user construct a value
-- of that type directly. One common practice is to just export the type and
-- leave the responsibility to creating types to module specific functions, just
-- like is done in the Map module where we can't construct a Map directly, but
-- can use `fromList` to create one. Like we are doing below by exporting Shape,
-- and leaving the construction of a point to the `createShape` function, but it
-- seems to not be working well, when loading types.hs into GHCi I still can
-- create Circles directly. TODO: Check this unespected behavior
, Shape
, createShape
)
where

import Data.Map hiding (map, singleton, foldr)

-- We can constructor our own data types by using the `data` keyword, we have to
-- specify the type constructor and then the value constructors that can be used for
-- that type, which values that type can assume. The value constructors are separated
-- by an `|` which means or, so for Int we can have `...-2 | -1 | 0 | 1 | 2 ...`.
-- Lets defined a shape type which can have the values of Circle and Rectangle, wherein
-- we have fields, of specific types, to contain some values. So for a circle we have
-- a X, Y and a Radius, and for a Rectangle we have X1, Y1, X2, Y2 for its corners
data Shape = Circle Float Float Float | Rectangle Float Float Float Float deriving (Show)

createShape :: [Char] -> Float -> Float -> Float -> Shape
createShape "circle" x y z = Circle x y z

-- Those value constructor are actually like functions and the fields are function parameters.
-- But its resulting value is threated as any other value, so we can pattern match
-- against it normally. Notice that on type declarations/annotations we define the types
-- not the values, so when defining a function that works on shapes we define it as
-- `... -> Shape -> ...`, we use the type constructor not the values contructors.
-- Although when pattern matching we must use values, so here is the place for value
-- constructors
surface :: Shape -> Float
surface (Circle _ _ r) = pi * r ^ 2
surface (Rectangle x1 y1 x2 y2) = (abs $ x2 - x1) * (abs y2 - y1)

-- Given that value constructors are just like functions we can map them over things,
-- partial apply some parameters, send as function to other things ...
circles :: [Shape]
circles = map (Circle 10 20) [4,5,6,6]

-- Being able to construct our own types leverage us to create more complex types
-- by composing types into other types. Obs.: Type constructors and value constructors,
-- can have the same name, there isn't any restriction on that, the only restriction
-- is that we must have a specific name for each value constructor
data Point = Point Float Float
data Cuboid = Cuboid Point Point

-- Lets say that you want to define a person type with a firstName, lastName, age, height ...
-- all of those specified using fields. It would be boring, now imagine creating
-- functions, with meaningful names, to extract the specific values from each field,
-- that would turn into something almost impossible to change and very error prone.
-- To work around that we have the record notation to define types, by using it Haskell
-- will automatically defined those functions, and in the declaration we are able to
-- see that which field represents
data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     , height :: Float
                     , phoneNumber :: String
                     , flavor :: String
                     } deriving (Show)
-- You can pattern match on record types by using `Person { firstName = x }`, with
-- only the fields that you want, and rename them by using `=` if desired. To create a new
-- value when using the record syntax you have to specify all of its fields by order like
-- any other value constructor with fields, or specify each of them by using the
-- syntax above with every field
-- `Person { firstName = "a", lastName = "b", age = 1, height = 1.0, phoneNumber = "c", flavor = "d"}`,
-- although we can omit some fields however when evaluating them it would result in a runtime error

-- Imagine that you want to define the type for arrays (already defined), how would
-- you define every possible construction of types inside of the array? It would
-- be impossible to figure out every single combination that a user can create.
-- And for the `Maybe` type the same applies, so for that we have the type parameters
-- which let the type constructor receive another type as parameter and apply that
-- on its value constructors, here is the example for `Maybe`
-- data Maybe a = Nothing | Just a
-- Although `Nothing` doesn't use the parameter it belongs to the `Maybe a` and that
-- is a explicit `a`, it isn't a placeholder for something, due to that we can fit
-- `Nothing` in any type declaration that uses `Maybe X`. On the other hand when
-- we say `Just 'a'` that value belongs to `Maybe Char` because its type parameter
-- is filled with an char, and the same applies for `:t Just "Xasd" --> Maybe [Char]`,
-- `:t Just 1 --> (Num a) => Maybe a` notice that even in the last we saw `Maybe a`
-- that `a` is an instance of `Num a` as specified in the type constraints.
-- Another to pay attention is when defining functions that works on types that have
-- parameters, if we want a specific type contained on that type we must specify the
-- parameter type on the function's type definition, if we don't care about that we
-- can put type variable
isNull :: Maybe a -> Bool
isNull Nothing = True
isNull _ = False

-- One thing that we can but it isn't a good practice is to put type constraints on
-- the data type definition, lets say that for Map we want to ensure that the is
-- able to be ordered, we can write the following type definition
-- `data (Ord k) => Map k v = Pair k v`
-- But it was considered a misfeature because that leads to putting that constraint
-- on every fucking function that manipulates a map, that something can lead to
-- noise on type declarations, check [this](https://wiki.haskell.org/Data_declaration_with_constraint)

-- One feature that you could have noticed is the use of `data ... = ... deriving (Show)`
-- this is for when you want to implement a common behavior that can auto implements
-- based on the fields of your data type. They all have some rules for applying and
-- on top of those its function will be implemented, e.g., for `Show, Read` all of your
-- fields - whether you have, when you don't they just use a stringify on your value
-- constructor - have to be part of those typeclasses. In the other hand some of them are based
-- on the order of the value constructors like `Ord`, which gets the declaration order
-- and assumes that they are ascending, for `Enum` that considers the order as successors
-- and predecessors, for Bounded that checks the limits and implements based on that
-- , for .... Each one of them gonna have a specific rule set for working with the
-- type declarations.
data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
    deriving (Eq, Ord, Show, Read, Bounded, Enum)

-- Bounded type class
weekStart = minBound :: Day
weekEnd = maxBound :: Day

-- Eq type class
isTuesday day = day == Tuesday

-- Ord type class
hasWeekJustStarted day = day < Wednesday
isWeekEnding day = day > Wednesday
isSunday day = day `compare` Sunday

-- For show you have to test on GHCi
-- Monday

-- Read type class - remember to use an explicit type annotation because of read is polymorphic
toDay str = read str :: Day

-- Enum type class
before day = pred day
after day = succ day

-- Imagine how it would be boring to write `[Char]` every time that you want a `String`
-- for that we have the type synonyms, that create aliases for other types
type PhoneList = [(String, String)]

-- As with the data type declarations we can compose type synonyms as well, to give
-- more meaning to our synonyms. One thing to remember that weren't mentioned is
-- that types must start with a upper letter and everything that starts with a
-- lower case letter is for declarations or type variables
type PhoneNumber = String
type Name = String
type PhoneBook = [(Name, PhoneNumber)]

phoneBook :: PhoneBook -- or `phoneBook :: PhoneList` or `phoneBook :: PhoneBook`
phoneBook =
    [("betty","555-2938")
    ,("bonnie","452-2928")
    ,("patsy","493-2928")
    ,("lucille","205-2928")
    ,("wendy","939-8282")
    ,("penny","853-2492")
    ]

-- Type synonyms can also be parameterized as data declarations, for example on an
-- Association List like the one before that takes both as String
type AssocList k v = [(k, v)]

-- Just like we can partially apply functions to get new functions, we can partially
-- apply type parameters and get new type constructors from them
type IntMap v = Map Int v
type CurriedIntMap = Map Int
-- Whether we had imported Data.Map as qualified we would have to declare it as
-- `type IntMap = Map.Map Int`

-- Creating type synonyms don't create new value constructors, we can't do things
-- like `AssocList [(1,2),(4,5),(7,9)]` because we use value constructors for that
-- and type synonyms just create type constructors, that are for type declarations/annotations.
-- All it means is that we can refer to its type by using different names
assocList = [(1,2),(3,5),(8,9)] :: AssocList Int Int
assocList' = [(1,2),(3,5),(8,9)] :: [(Int, Int)]

-- Sometimes we have to define the data structures in a way that they are composed by
-- itself, like in a list where [5] is just syntatic sugar for 5:[]. Here we define the List type
-- using algebraic data types (data declarations) in a recursive way
data List a = Empty | Cons a (List a) deriving (Show, Read, Eq, Ord)
recursiveList = Cons 5 Empty
recursiveList' = 5 `Cons` Empty
longRecursiveList = 3 `Cons` (4 `Cons` (5 `Cons` Empty))

-- We could simplify that by using the value constructor as an operator. Functions are considered
-- operators when you define them by only special characters, since value constructors
-- are just functions we can do that also. Notice also that mostly when defining operators we have
-- to specify the precedence and associativeness type by using `infixl` (left-associativity),
-- `infixr` (right-associativity), `infix` (non-associativity, like ==, we can't do like 1 == 1 == 1,
-- they should be grouped by hand). Then after the associativeness type we put the precedence
-- and then the operator name, you can think of associativity as grouping rule, group from left,
-- group from right, don't group; and for precedence just think about `+` (infixr 6 +) and `*` (infixr 7 *) -
-- higher precedence means apply first
infixr 5 :-: -- group from right and only after applying everything from 9 to 5
data List' a = Empty' | a :-: (List' a) deriving (Show, Read, Eq, Ord)
longRecursiveList' = 3 :-: 4 :-: 5 :-: Empty'

data BinaryTree a = EmptyBT | Node a (BinaryTree a, BinaryTree a) deriving (Show, Read, Eq)

singleton :: a -> BinaryTree a
singleton x = Node x (EmptyBT, EmptyBT)

treeInsert :: (Ord a) => a -> BinaryTree a -> BinaryTree a
treeInsert x EmptyBT = singleton x
treeInsert x tree@(Node val (left, right))
    | x == val = tree
    | x < val = Node val ((treeInsert x left), right)
    | x > val = Node val (left, (treeInsert x right))

treeElem :: (Ord a) => a -> BinaryTree a -> Bool
treeElem _ EmptyBT = False
treeElem x (Node val (left, right))
    | x == val = True
    | x < val = x `treeElem` left
    | x > val = x `treeElem` right

tree :: BinaryTree Int
tree = let nums = [8,6,4,1,7,3,5]
           in foldr treeInsert EmptyBT nums

hasEight = 8 `treeElem` tree
hasOneHundred = 100 `treeElem` tree
hasOne = 1 `treeElem` tree

-- Here you can see an instance of the functor type class for the BinaryTree type, it
-- simply applies the given function recursively througout the nodes until it
-- hits a `EmptyBT`. This instance is made possible because the Functor typeclass
-- requires a kind of `* -> *` and BinaryTree has it, since it receives the node type on
-- its declaration.
instance Functor BinaryTree where
    fmap f EmptyBT = EmptyBT
    fmap f (Node x (left, right)) = Node (f x) ((fmap f left), (fmap f right))

incrementedTree :: BinaryTree Int
incrementedTree = fmap (+ 1) tree

-- In the applicatives section we have seen that a list can have two behavior when
-- acting as an applicative, one is to create a cartesian product of the values,
-- whilst the other is to apply the left to the corresponding in the right, like a zip.
-- The first is the default intance for [] while the latter is the ZipList instance.
-- Given that ZipList is just a type contructor which receives an array and declares
-- the necessary instances to work with applicatives, we could create a new type for that
-- `data ZipList a = ZipList [a]` or use the record syntax to create the unwrapper
-- `data ZipList a = ZipList { getZipList :: [a] }. By declaring the new type that is
-- just a wrapper for an array we could create two instances for the same data type.
-- This is a very common patterns and is subjected to optimizations since
-- we just want a wrapper.
-- For that Haskell has the `newtype` keyword which creates such wrappers in a faster
-- way, since inside it wouldn't have to unwrap and wrap the inner type inside the new type.
-- With `newtype` Haskell creates like an alias to our type, removing the needs of
-- wrapping, this is possible because the internal type is the same as one of which
-- Haskell already knows how to work.
-- Below you can see the creation of a newtype for tuples of two elements, and using
-- it to create a new instance of Functor in a way that the first element gets mapped
-- instead of the second
newtype Pair b a = Pair { getPair :: (a,b) }
instance Functor (Pair c) where
    fmap f (Pair (x, y)) = Pair (f x, y)
-- This new type creation wasn't done just to map the first parameter, observe that
-- the type variables in the type constructor are inverted when compared with the field
-- in the value constructor. That had to be done because the Functor class accepts only
-- types with one type variable, and tuples has two, making one of them to be locked,
-- to be rigid. This way partially applying the type constructor and making the fmap
-- lock the type in the type definition of fmap, with the type variables not inverted we
-- would get `fmap :: (a -> b) -> Pair c a -> Pair c b` which internally would translate
-- c to the first component of the tuple and a/b to the second, making the change to happen
-- in the second, and limiting us to implement it as a change in the first because there
-- is no way `a -> b` in a way that it receives a `c` and returns `c`. To work around
-- that we invert the type variables in the constructor, this way when locking the first
-- type of Pair you are actually locking the second component of the inner tuple,
-- making it possible to map the first instead of the second. The type signature
-- remains the same, but when creating the Pair using the value constructor the types
-- would match normally since the *c* refers to the *b* in `Pair (a, b)` and the *a*
-- is determined by the function, making it polymorphic.

-- You may be asking yourself why not using newtype for everything. The problem is that
-- types declared with newtype can have only one value constructor, with exactly one field.
-- This is a good limitation since newtypes should be used for wrapping other types,
-- why would you want to add a field to a wrapper? By doing that you would be incrementing
-- the internal type, making the outer to be not just a wrapper. Such "limitation" leads
-- us to a pattern where we declare all the new types with record syntax, on it having
-- just one field called get<Type>, which will create a function for unwrapping the
-- internal type since we are using the record syntax. However ZipList doesn't have
-- a instance for show, we can create new types that derives from type classes,
-- since we follow all the constraints for deriving, like that all the fields have
-- to be instances also
newtype CharList a = CharList { getCharList :: [a] } deriving (Show, Eq, Ord)
bIsMoreThanA = CharList "B" > CharList "A"
bAsCharList = show (CharList "B")
bIsNotB = CharList "b" /= CharList "B"
